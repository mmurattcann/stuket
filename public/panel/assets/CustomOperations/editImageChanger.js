$(function () {

    $('.file-input-area').hide();

    $('input:radio[name="profilePictureRadio"]').change(
        function(){
            if ($(this).is(':checked') && $(this).val() == 2) {

                $('.file-input-area').show();
                $('.original-image').hide();

            }else  if ($(this).is(':checked') && $(this).val() == 1) {

                $('.file-input-area').hide();
                $('.original-image').show();
                $('input[name="image"]').val("")
            }
        });
})
