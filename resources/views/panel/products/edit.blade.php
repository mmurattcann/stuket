@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection
@push("css")
    <!-- DROPZone -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />

    <!-- forn-wizard css-->
    <link href="{{panelAsset("plugins/forn-wizard/css/material-bootstrap-wizard.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/forn-wizard/css/demo.css")}}" rel="stylesheet" />


    <!---Sweetalert Css-->
    <link href="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/sweet-alert/sweetalert.css")}}" rel="stylesheet" />
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"> {{$title}}</div>
                </div>
                <div class="card-body p-6">
                    <div class="wizard-container">
                        <div class="wizard-card m-0"  id="wizardProfile">

                            <div class="wizard-navigation">
                                <ul>
                                    <li><a href="#infos" data-toggle="tab">İlan Bilgileri</a></li>
                                    <li><a href="#photos" data-toggle="tab">İlan Fotoğrafları</a></li>
                                </ul>
                            </div>


                            <div class="tab-content">
                                <div class="tab-pane" id="infos">
                                    <div class="row">
                                        <form action="{{route("products.update", $product->id)}}" id="product-form" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            @method("PUT")
                                            <input type="hidden" name="is_active" value="1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Üniversite</label>
                                                        <select class="form-control select2 custom-select" id="university_id" name="university_id">

                                                            <option value="">--- Seçim Yapın ---</option>
                                                            @foreach($universities as $university)
                                                                <option value="{{$university->id}}"
                                                                @if($product->university_id == $university->id) selected @endif>{{$university->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error("university_id")
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Fakülte</label>
                                                        <select class="form-control select2 custom-select" id="faculty_id" name="faculty_id">

                                                            <option value="">--- Seçim Yapın ---</option>
                                                            @foreach($faculties as $faculty)
                                                                <option value="{{$faculty->id}}"
                                                                        @if($product->faculty_id == $faculty->id) selected @endif>
                                                                    {{$faculty->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error("faculty_id")
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Kategori</label>
                                                        <select class="form-control select2 custom-select" id="category_id" name="category_id">

                                                            <option value="">--- Seçim Yapın ---</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}" @if($category->id == $product->category->id) selected @endif>
                                                                    {{$category->title}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @error("category_id")
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Başlık</label>
                                                        <input type="text" class="form-control" name="title" placeholder="ÖRN: Satılık Telefon" value="{{old("title", $product->title)}}">
                                                        @error("title")
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="form-label">Slug (SEO URL için)</label>
                                                            <input type="text" class="form-control" name="slug" placeholder="ÖRN: satilik-telefon" value="{{old("slug", $product->slug)}}">
                                                            @error("slug")
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="form-label">Fiyat</label>
                                                            <input type="text" class="form-control" name="price" placeholder="ÖRN: 250" value="{{old("price", $product->price)}}">
                                                            @error("price")
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Açıklama</label>
                                                    <textarea name="description" class="form-control">{{old("description", $product->description)}}</textarea>
                                                    @error("description")
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Adres</label>
                                                    <textarea name="address" class="form-control">{{old("address", $product->address)}}</textarea>
                                                    @error("address")
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="photos">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <form action="{{route("product-images.store")}}" class="dropzone" id="dropzone" name="image-form" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method("POST")

                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <div class="dz-message">
                                                    <div class="col-xs-8">
                                                        <div class="message">
                                                            <h4>Yüklemek istediğiniz dosyaları buraya sürükleyip bırakın</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple />
                                                </div>

                                            </form>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="button" id="upload-button" class="btn btn-primary pull-right mt-3"><i class="fa fa-upload"></i>  Yükle</button>
                                        </div>

                                    </div><br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-md-12">
                                            <h4>Mevcut Görseller</h4>
                                            <table class="table table-bordered" id="images-table">
                                                <thead>
                                                <tr>
                                                    <th>#ID</th>
                                                    <th>Görsel</th>
                                                    <th>Sil</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($product->images as $image)
                                                    <tr id="image-tr-{{$image->id}}" data-test="Bulundu">
                                                        <td>{{$image->id}}</td>
                                                        <td>
                                                            <img src="{{getProductImage($image->image)}}" width="200" height="120">
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-outline-danger delete-button"
                                                                    data-id="{{$image->id}}"
                                                                    data-route="{{route("product-images.destroy", $image->id)}}" >
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="wizard-footer mt-5">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-primary btn-wd m-0' name='next' value='Sonraki' />
                                        <input type='submit' id="saveProduct" class='btn btn-finish btn-fill btn-success btn-wd m-0' value='Tamamla' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd m-0' name='previous' value='Önceki' />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>
                    </div> <!-- wizard container -->
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")
    <!-- forn-wizard js-->
    <script src="{{panelAsset("plugins/forn-wizard/js/material-bootstrap-wizard.js")}}"></script>
    <script src="{{panelAsset("plugins/forn-wizard/js/jquery.validate.min.js")}}"></script>
    <script src="{{panelAsset("plugins/forn-wizard/js/jquery.bootstrap.js")}}"></script>
    <script src="{{panelAsset("plugins/bootstrap-wizard/jquery.bootstrap.wizard.js")}}"></script>
    <script src="{{panelAsset("plugins/bootstrap-wizard/wizard.js")}}"></script>
    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>

    <!-- Sweet alert Plugin -->
    <script src="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.js")}}"></script>
    <script src="{{panelAsset("plugins/sweet-alert/sweetalert.min.js")}}"></script>
    <script src="{{panelAsset("js/sweet-alert.js")}}"></script>


    <script src="{{panelAsset("js/dropzone.js")}}"></script>
    <script src="{{panelAsset("CustomOperations/productDZUpload.js")}}"></script>
    <script>
        $(function () {
            $(".table tbody").on("click",".delete-button", function () {

                var id = $(this).data("id");

                let $tr = $("#image-tr-"+id);

                var $route = $(this).data("route");
                swal({
                        title: 'Silmek istediğinize emin misiniz?',
                        text: "Bilgiler kalıcı olarak silinecek!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Evet, sil!',
                        cancelButtonText: 'Vazgeç!',
                        reverseButtons: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            var token = $("meta[name='csrf-token']").attr("content");
                            $.ajax({
                                url: $route,
                                type: 'DELETE',
                                data: {
                                    "id": id,
                                    "_token": token,
                                },
                                success: function (data) {
                                   $("#images-table  tbody").find($tr).fadeOut();

                                    toastr.options.closeButton = false;
                                    toastr.options.preventDuplicates = false;
                                    toastr.options.closeMethod = 'fadeOut';
                                    toastr.options.closeDuration = 100;
                                    toastr.success(data.message);
                                }
                            });
                        }
                    }
                )
            })
        })
    </script>
@endpush
