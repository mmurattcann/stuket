@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")

@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">

    </ol>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("users.update", $user->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("PUT")
                                <div class="form-group">
                                    <label class="form-label">Profil Resmi</label>
                                    <img src="{{getProfileImage($user->image)}}" class="original-image" alt="{{$user->name}}" style="width:100px;height: 100px; ">
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="profilePictureRadio" value="1" checked>
                                        <span class="custom-control-label">Orijinali Koru</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="profilePictureRadio" value="2">
                                        <span class="custom-control-label">Değiştir</span>
                                    </label>
                                </div>
                                <div class="form-group file-input-area">
                                    <div class="form-label">Profil Resmini Seç</div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image">
                                        <label class="custom-file-label">Seç</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Ad Soyad</label>
                                    <input type="text" class="form-control" name="name" placeholder="Ad Soyad" value="{{old("name", $user->name)}}">
                                    @error("name")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">E-Posta</label>
                                    <input type="email" class="form-control" name="email" placeholder="E-Posta Adresi" value="{{old("email", $user->email)}}">
                                    @error("email")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Telefon Numarası</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Telefon Numarası" value="{{old("phone", $user->phone)}}">
                                    @error("phone")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Hakkında</label>
                                    <textarea name="about" class="form-control">{{old("about", $user->about)}}</textarea>
                                    @error("about")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Şifre</label>
                                    <input type="password" class="form-control" name="password" placeholder="Şifre" >
                                    @error("password")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Şifre Tekrar</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Şifre Tekrar">
                                    @error("password_confirmation")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Rol</label>
                                    <select name="role" class="form-control">
                                        <option value="0" @if($user->role == 0) selected @endif >Standart Kullanıcı</option>
                                        <option value="1" @if($user->role == 1) selected @endif>Admin</option>
                                    </select>
                                </div>
                                {{--<div class="form-group ">
                                    <label class="form-label">Kullanıcı Rolü</label>
                                    <select class="form-control select2 custom-select" name="role">
                                        <option value="{{$standardUserEnum}}" @if($user->role == $standardUserEnumString) selected  @endif >{{$standardUserEnumString}}</option>
                                        <option value="{{$adminEnum}}" @if($user->role == $adminEnumString) selected @endif >{{$adminEnumString}}</option>
                                    </select>
                                    @error("role")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
--}}
                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")

    <script>
        $(function () {

            $('.file-input-area').hide();

            $('input:radio[name="profilePictureRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.file-input-area').show();
                        $('.original-image').hide();

                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.file-input-area').hide();
                        $('.original-image').show();
                        $('input[name="image"]').val("")

                    }
                });
        })
    </script>
@endpush
