@extends("layouts.panel._layout")
@section("title")
    {{$title}}
@endsection

@push("css")

    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />
    <!-- WYSIWYG Editor css -->
    <link href="{{panelAsset('plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("sliders.store")}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("POST")
                                <input type="hidden" name="is_active" value="1">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-label">Görsel</div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image">
                                                <label class="custom-file-label">Seç</label>
                                            </div>

                                            @error("image")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label">Üst Başlık</label>
                                        <input type="text" class="form-control" name="top_title" placeholder="Üst Başlık" value="{{old("top_title")}}">
                                        @error("top_title")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label">Başlık</label>
                                        <input type="text" class="form-control" name="title" placeholder="Başlık" value="{{old("title")}}">
                                        @error("title")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label">Sıra</label>
                                        <input type="number" min="1" class="form-control" name="rank" placeholder="Sıra Numarası" value="{{old("rank")}}">
                                        @error("rank")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label">Açıklama</label>
                                        <textarea class="form-control" name="description" placeholder="Açıklama">{{old("description")}}</textarea>
                                        @error("description")
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Buton Kullanılsın mı?</label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input dont-use-button" name="has_button" value="0" checked>
                                                <span class="custom-control-label">Kullanılmasın</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input use-button" name="has_button" value="1" >
                                                <span class="custom-control-label dont-use-button">Kullanılsın</span>
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                <div class="row slider-button-group">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">Buton Başlığı</label>
                                            <input type="text" class="form-control" name="button_title" placeholder="Buton Başlığı" value="{{old("button_title")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">Buton URL'i</label>
                                            <input type="text" class="form-control" name="url" placeholder="URL" value="{{old("url")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-gro">
                                            <label class="form-label">Buton Class</label>
                                            <select name="button_class" class="form-control">
                                                <optgroup label="Normal Butonlar">
                                                    @foreach($normalButtonClasses as $normalButtonClass)
                                                        <option
                                                            value="{{$normalButtonClass}}">
                                                            {{$normalButtonClass}}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                                <optgroup label="Outline Butonlar">
                                                    @foreach($outlinedButtonClasses as $outlinedButtonClass)
                                                        <option
                                                            value="{{$outlinedButtonClass}}">
                                                            {{$outlinedButtonClass}}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                                <option></option>

                                            </select>
                                        </div>

                                        {{--  <div class="form-group">
                                              <label class="form-label">Buton Class(<small> Bootstrap</small>)</label>
                                              <input type="text" class="form-control" name="button_class" placeholder="Buton Classı" value="{{old("button_class", $slider->button_class)}}">
                                          </div>--}}
                                    </div>
                                </div>
                               {{-- <div class="form-group ">
                                    <label class="form-label">Kullanıcı Rolü</label>
                                    <select class="form-control select2 custom-select" name="role">

                                        <option value="{{$standardUserEnum}}">Standart Kullanıcı</option>
                                        <option value="{{$adminEnum}}">Admin</option>
                                    </select>
                                    @error("role")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>--}}
                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("js")

    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>
    <!-- WYSIWYG Editor js -->
    <script src="{{panelAsset('plugins/wysiwyag/jquery.richtext.js')}}"></script>
    <script src="{{panelAsset('plugins/wysiwyag/richText1.js')}}"></script>
    <script>
        $(function () {
            let $useButton = $(".use-button");
            let $dontUseButton = $(".dont-use-button");
            let $sliderButtonGroup = $(".slider-button-group");

            let $titleInput = $('input[name="button_title"]');
            let $urlInput = $('input[name="url"]');
            let $classInput = $('input[name="button_class"]');


            $sliderButtonGroup.hide();
            $($useButton).click(function () {
                $sliderButtonGroup.fadeIn();
            })

            $($dontUseButton).click(function () {
                $sliderButtonGroup.hide();
                $titleInput.val("");
                $urlInput.val("");
                $classInput.val("");
            })
        })
    </script>
@endpush
