@extends("layouts.panel._layout")

@section("title")
{{$title}}
@endsection

@push("css")

@endpush

@section("content")
    <div class="side-app">
        <div class="page-header">
            <h4 class="page-title">{{$title}}</h4>
            <ol class="breadcrumb">

            </ol>
        </div>

        <div class="row row-cards">
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12">
                <div class="card card-counter bg-gradient-primary shadow-primary">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="mt-4 mb-0 text-white">
                                    <h3 class="mb-0">{{$userCount}}</h3>
                                    <p class="text-white mt-1">Toplam Kullanıcı </p>
                                </div>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-bar-chart mt-3 mb-0"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12">
                <div class="card card-counter bg-gradient-secondary shadow-secondary">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="mt-4 mb-0 text-white">
                                    <h3 class="mb-0">{{$productCount}}</h3>
                                    <p class="text-white mt-1">Toplam Ürün </p>
                                </div>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-send-o mt-3 mb-0"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12">
                <div class="card card-counter bg-gradient-warning shadow-warning">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="mt-4 mb-0 text-white">
                                    <h3 class="mb-0">{{$blogCount}}</h3>
                                    <p class="text-white mt-1">Toplam Blog</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-mail-reply mt-3 mb-0"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12">
                <div class="card card-counter bg-gradient-success shadow-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="mt-4 mb-0 text-white">
                                    <h3 class="mb-0">{{$universityCount}}</h3>
                                    <p class="text-white mt-1">Toplam Üniversite</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-money mt-3 mb-0"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cards">
            <div class="col-xl-6 col-md-6 col-sm-6 col-lg-6">
                <div class="card">
                    <div class="card-header ">
                        <h4>Son Eklenen Kullanıcılar</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive service">
                            <table class="table table-bordered table-hover  mb-0 text-nowrap ">
                                <thead>
                                <tr>
                                    <th>Avatar</th>
                                    <th>Ad</th>
                                    <th>Rol</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lastUsers as $lastUser)
                                <tr>
                                    <td>
                                        <img src="{{getProfileImage($lastUser->image)}}"
                                             alt="{{$lastUser->name}}"
                                             width="50"
                                             height="50">
                                    </td>
                                    <td>{{$lastUser->name}}</td>
                                    <td>ROL</td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="text-center">
                                    <td colspan="3">
                                        <a href="{{route("users.index")}}" class="btn btn-primary">Tüm Kullanıcıları Görüntüle</a>
                                    </td>
                                </tr>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-6 col-lg-6">
                <div class="card">
                    <div class="card-header ">
                        <h4>Son Eklenen Ürünler</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive service">
                            <table class="table table-bordered table-hover  mb-0 text-nowrap ">
                                <thead>
                                <tr>
                                    <th>Kapak</th>
                                    <th>Başlık</th>
                                    <th>İlan Sahibi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lastProducts as $lastProduct)
                                <tr>
                                    <td>
                                        <img src="{{getCoverImage($lastProduct->id)}}"
                                             alt="{{$lastProduct->title}}"
                                             width="50"
                                             height="50">
                                    </td>
                                    <td>{{$lastProduct->title}}</td>
                                    <td>{{$lastProduct->owner->name}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="text-center">
                                    <td colspan="3">
                                        <a href="{{route("products.index")}}" class="btn btn-primary">Tüm Ürünleri Görüntüle</a>
                                    </td>
                                </tr>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push("js")

@endpush
