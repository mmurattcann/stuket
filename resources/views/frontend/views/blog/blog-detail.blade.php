@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")

@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li class='active'>Blog Details</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="row">
                <div class="blog-page">
                    <div class="col-md-9">
                        <div class="blog-post wow fadeInUp" style="height: auto">
                            <img class="img-responsive" src="{{getBlogCover($blog->image)}}" alt="{{$blog->title}}">
                            <h1>{{$blog->title}}</h1>
                            <span class="author">John Doe</span>
                            <span class="review">{{$blog->comments->count()}} Yorum</span>
                            <span class="date-time">{{$blog->modified_date}}</span>
                            <p>{!! $blog->content !!}</p>
                            <div class="social-media">
                                <span>share post:</span>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#" class="hidden-xs"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                        <div class="blog-post-author-details wow fadeInUp">
                            <div class="row">
                                <div class="col-md-2 col-xs-12 col-sm-12">
                                    <img src="assets/images/testimonials/member3.png" alt="Responsive image"
                                         class="img-circle img-responsive">
                                </div>
                                <div class="col-md-10">
                                    <h4>John Doe</h4>
                                    <span class="author-job">Web Designer</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                </div>
                            </div>
                        </div>

                        </div>
                        <div class="col-md-3 sidebar">


                            <div class="sidebar-module-container">
                                <div class="home-banner outer-top-n outer-bottom-xs">
                                    <img src="{{frontAsset('images/banners/LHS-banner.jpg')}}" alt="Image">
                                </div>
                                <!-- ==============================================CATEGORY============================================== -->
                            @include("frontend.partials.category")
                            <!-- ============================================== CATEGORY : END ============================================== -->
                                <div class="sidebar-widget outer-bottom-xs wow fadeInUp">
                                    <h3 class="section-title">Tab widget</h3>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#popular" data-toggle="tab">popular post</a></li>
                                        <li><a href="#recent" data-toggle="tab">recent post</a></li>
                                    </ul>
                                    <div class="tab-content" style="padding-left:0">
                                        <div class="tab-pane active m-t-20" id="popular">
                                            @foreach($similarPosts as $post)
                                                <div class="blog-post">
                                                    <img class="img-responsive"
                                                         src="{{getBlogCover($post->image)}}"
                                                         alt="">
                                                    <h4>
                                                        <a href="{{route("front.blog-detail", $post->slug)}}">{{$post->title}}</a>
                                                    </h4>
                                                    <span class="review">{{$blog->comments->count()}} Yorum </span>
                                                    <span class="date-time">{{$post->modified_date}}</span>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="tab-pane m-t-20" id="recent">
                                            <div class="blog-post inner-bottom-30">
                                                <img class="img-responsive"
                                                     src="assets/images/blog-post/blog_big_03.jpg"
                                                     alt="">
                                                <h4><a href="blog-details.html">Simple Blog Post</a></h4>
                                                <span class="review">6 Comments</span>
                                                <span class="date-time">5/06/18</span>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

                                            </div>
                                            <div class="blog-post">
                                                <img class="img-responsive"
                                                     src="assets/images/blog-post/blog_big_01.jpg"
                                                     alt="">
                                                <h4><a href="blog-details.html">Simple Blog Post</a></h4>
                                                <span class="review">{{$blog->comments->count()}}</span>
                                                <span class="date-time">10/07/18</span>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ============================================== PRODUCT TAGS ============================================== -->
                                <div class="sidebar-widget product-tag wow fadeInUp">
                                    <h3 class="section-title">Product tags</h3>
                                    <div class="sidebar-widget-body outer-top-xs">
                                        <div class="tag-list">
                                            <a class="item" title="Phone" href="category.html">Phone</a>
                                            <a class="item active" title="Vest" href="category.html">Vest</a>
                                            <a class="item" title="Smartphone" href="category.html">Smartphone</a>
                                            <a class="item" title="Furniture" href="category.html">Furniture</a>
                                            <a class="item" title="T-shirt" href="category.html">T-shirt</a>
                                            <a class="item" title="Sweatpants" href="category.html">Sweatpants</a>
                                            <a class="item" title="Sneaker" href="category.html">Sneaker</a>
                                            <a class="item" title="Toys" href="category.html">Toys</a>
                                            <a class="item" title="Rose" href="category.html">Rose</a>
                                        </div><!-- /.tag-list -->
                                    </div><!-- /.sidebar-widget-body -->
                                </div><!-- /.sidebar-widget -->
                                <!-- ============================================== PRODUCT TAGS : END ============================================== -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push("js")

@endpush
