@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")

@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="contact-page">

                <div class="row">

                    <div class="col-md-12 contact-map outer-bottom-vs">
                       {!! $googleMap !!}
                    </div>
                    <form action="{{route("front.contact")}}" method="POST">
                        @csrf
                        @method("POST")
                        <div class="col-md-9 contact-form">
                        <div class="col-md-12 contact-title">
                            <h4>İletişim Formu</h4>
                        </div>
                        <div class="col-md-4 ">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Adınız <span>*</span></label>
                                    <input type="text" name="name" class="form-control unicase-form-control text-input" id="exampleInputName" placeholder="" required>
                                </div>
                        </div>
                        <div class="col-md-4">

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">E-Posta<span>*</span></label>
                                    <input type="email" name="email" required class="form-control unicase-form-control text-input" id="exampleInputEmail1" placeholder="">
                                </div>

                        </div>
                        <div class="col-md-4">

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputTitle">Konu <span>*</span></label>
                                    <input type="text" name="subject" required class="form-control unicase-form-control text-input" id="exampleInputTitle" placeholder="Title">
                                </div>
                        </div>
                        <div class="col-md-12">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputComments">Mesajınız <span>*</span></label>
                                    <textarea name="message" required class="form-control unicase-form-control" id="exampleInputComments"></textarea>
                                </div>

                        </div>
                        <div class="col-md-12 outer-bottom-small m-t-20">
                            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Gönder</button>
                        </div>
                    </div>
                    </form>
                    <div class="col-md-3 contact-info">
                        <div class="contact-title">
                            <h4>İletişim Bilgileri</h4>
                        </div>
                        <div class="clearfix address">
                            <span class="contact-i"><i class="fa fa-map-marker"></i></span>
                            <span class="contact-span">{{$siteAddress}}</span>
                        </div>
                        <div class="clearfix phone-no">
                            <span class="contact-i"><i class="fa fa-mobile"></i></span>
                            <span class="contact-span">{{$phoneNumber}}<br>{{$mobilePhoneNumber}}</span>
                        </div>
                        <div class="clearfix email">
                            <span class="contact-i"><i class="fa fa-envelope"></i></span>
                            <span class="contact-span"><a href="mailto:{{$contactEmail}}">{{$contactEmail}}</a></span>
                        </div>
                    </div>			</div><!-- /.contact-page -->
            </div><!-- /.row -->
            	</div><!-- /.container -->


        <!-- For demo purposes – can be removed on production -->


        <!-- For demo purposes – can be removed on production : End -->

        <!-- JavaScripts placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>

        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>

        <script src="assets/js/echo.min.js"></script>
        <script src="assets/js/jquery.easing-1.3.min.js"></script>
        <script src="assets/js/bootstrap-slider.min.js"></script>
        <script src="assets/js/jquery.rateit.min.js"></script>
        <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
        <script src="assets/js/bootstrap-select.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/scripts.js"></script>

        <!-- For demo purposes – can be removed on production -->

        <script src="switchstylesheet/switchstylesheet.html"></script>

        <script>
            $(document).ready(function(){
                $(".changecolor").switchstylesheet( { seperator:"color"} );
                $('.show-theme-options').click(function(){
                    $(this).parent().toggleClass('open');
                    return false;
                });
            });

            $(window).bind("load", function() {
                $('.show-theme-options').delay(2000).trigger('click');
            });
        </script>
        <!-- For demo purposes – can be removed on production : End -->



@endsection

@push("js")

@endpush
