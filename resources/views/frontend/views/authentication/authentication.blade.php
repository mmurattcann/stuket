@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")

@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>Giriş Yap & Kaydol</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="sign-in-page">
                <div class="row">
                    <!-- Sign-in -->

                    <div class="col-md-6 col-sm-6 sign-in">
                        <h4 class="">Giriş Yap</h4>
                        <p class="">Formu doldurarak giriş işlemini gerçekleştirin.</p>
                       @include("frontend.partials.success-message")
                       @if($message = Session::get("loginError"))
                            <div class="alert alert-danger"><p>{{$message}}</p></div>
                        @endif

                        <form action="{{route("front.login")}}" method="POST" class="register-form outer-top-xs" role="form">
                            @csrf
                            @method("POST")
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">E-Posta<span>*</span></label>
                                <input type="email" name="email" value="{{old("email")}}" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
                                @error("email")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="exampleInputPassword1">Şifre <span>*</span></label>
                                <input type="password" name="password" class="form-control unicase-form-control text-input" id="exampleInputPassword1" >
                                @error("password")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="radio outer-xs">
                                <label>
                                    <input type="radio" name="remember_me" id="optionsRadios2" value="option2">Beni Hatırla!
                                </label>
                                <a href="#" class="forgot-password pull-right">Şifremi Unuttum</a>
                            </div>
                            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Giriş Yap</button>
                        </form>
                    </div>
                    <!-- Sign-in -->

                    <!-- create a new account -->
                    <div class="col-md-6 col-sm-6 create-new-account">

                        <h4 class="checkout-subtitle">Kaydol</h4>
                        <p class="text title-tag-line">Formu <strong>öğrenci e-postanız</strong> ile  doldurarak STUKET ailesine katılın.</p>
                        <div class="col-md-12">
                            @include("frontend.partials.error-message")
                        </div>

                        <form action="{{route("front.register")}}" method="POST" class="register-form outer-top-xs" role="form" enctype="multipart/form-data">
                            @csrf
                            @method("POST")
                            <div class="form-group col-md-12">
                                <label class="info-title" for="exampleInputEmail2">Profil Resmi<span>*</span></label>
                                <input type="file" name="image" value="{{old("image")}}" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
                                @error("image")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="info-title" for="exampleInputEmail2">E-Posta <span>*</span></label>
                                <input type="text" name="email" value="{{old("email")}}" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
                                @error("email")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="info-title" for="exampleInputEmail2">Email Uzantısı</label>
                                <input type="text" name="email_extension" value=".edu.tr"  readonly class="form-control unicase-form-control text-input" id="exampleInputEmail2" >

                            </div>
                            <div class="form-group col-md-12">
                                <label class="info-title" for="exampleInputEmail1">Ad Soyad <span>*</span></label>
                                <input type="text" name="name" value="{{old("name")}}" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
                                @error("name")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label class="info-title" for="exampleInputEmail1">Telefon<span>*</span></label>
                                <input type="text" name="phone" value="{{old("phone")}}" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
                                @error("phone")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label class="info-title" for="exampleInputEmail1">Şifre <span>*</span></label>
                                <input type="password" name="password" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
                                @error("password")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label class="info-title" for="exampleInputEmail1">Şifre (Tekrar) <span>*</span></label>
                                <input type="password" name="password_confirmation" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
                                @error("password_confirmation")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label class="info-title" for="exampleInputEmail1">Kısaca Kendinizden Bahsedin <span>*</span></label>
                                <textarea name="about" class="form-control">{{old("about")}}</textarea>
                                @error("about")
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Kaydol</button>
                        </form>


                    </div>
                    <!-- create a new account -->			</div><!-- /.row -->
            </div><!-- /.sigin-in-->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <div id="brands-carousel" class="logo-slider wow fadeInUp">

                <div class="logo-slider-inner">
                    <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                        <div class="item m-t-15">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item m-t-10">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->
                    </div><!-- /.owl-carousel #logo-slider -->
                </div><!-- /.logo-slider-inner -->

            </div><!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
    </div><!-- /.body-content -->
@endsection

@push("js")

@endpush
