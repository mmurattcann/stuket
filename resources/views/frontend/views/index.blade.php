@extends("layouts.frontend._layout")

@section("title") {{$title}} @endsection

@section("content")
    @include("frontend.partials.index-main-slider")
    <div class="container home-banner1">
        <div class="row row-bottom">
            <div class="col-sm-4 col-xs-12 col-md-4">
                <a class="btn btn-call btn-bottom" href="tel:0800 622 6225">Bizi Arayın!</a>
            </div>
            <div class="col-sm-4 col-xs-12 col-md-4">
                <a class="btn btn-email btn-bottom" href="mailto:info.stuket@gmail.com">E-Posta</a>
            </div>
        </div>
    </div>

    <div class="container">



        <!-- ============================================== INFO BOXES ============================================== -->
        <div class="info-boxes wow fadeInUp">
            <div class="info-boxes-inner">
                <div class="row">
                    <div class="header-benefit">
                        <div class="benefit-icon"><i class="fa fa-credit-card"></i></div>
                        <h4 class="info-box-heading green">30 days money back guarantee</h4>
                    </div>


                    <!-- .col -->
                    <div class="header-benefit">
                        <div class="benefit-icon"><i class="fa fa-truck"></i></div>
                        <h4 class="info-box-heading green">Free delivery on orders over $25</h4>
                    </div>



                    <!-- .col -->

                    <div class="header-benefit">
                        <div class="benefit-icon"><i class="fa fa-certificate"></i></div>
                        <h4 class="info-box-heading green">Extra $5 off on selected items</h4>
                    </div>

                    <!-- .col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.info-boxes-inner -->

        </div>
        <!-- /.info-boxes -->

        <!-- ============================================== FEATURED PRODUCTS ============================================== -->
        @include("frontend.partials.index-product-slider")
        <!-- /.section -->
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

        <div class="row">
            <!-- ============================================== CONTENT ============================================== -->
            <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">

                <!-- ============================================== BLOG SLIDER ============================================== -->
                @include("frontend.partials.index-blog-slider")
                <!-- /.section -->
                <!-- ============================================== BLOG SLIDER : END ============================================== -->



            </div>
            <!-- /.homebanner-holder -->
            <!-- ============================================== CONTENT : END ============================================== -->

            <!-- ============================================== SIDEBAR ============================================== -->
            <div class="col-xs-12 col-sm-12 col-md-3 sidebar">
                <!-- ============================================== Testimonials============================================== -->
                <div class="sidebar-widget testimonials-block">
                    <div id="advertisement" class="advertisement">
                        <div class="item">
                            <div class="avatar"><img src="assets/images/testimonials/member1.png" alt="Image"></div>
                            <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                            <div class="clients_author">John Doe <span>Abc Company</span> </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- /.item -->

                        <div class="item">
                            <div class="avatar"><img src="assets/images/testimonials/member3.png" alt="Image"></div>
                            <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                            <div class="clients_author">Stephen Doe <span>Xperia Designs</span> </div>
                        </div>
                        <!-- /.item -->

                        <div class="item">
                            <div class="avatar"><img src="assets/images/testimonials/member2.png" alt="Image"></div>
                            <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                            <div class="clients_author">Saraha Smith <span>Datsun &amp; Co</span> </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- /.item -->

                    </div>
                    <!-- /.owl-carousel -->
                </div>

                <!-- ============================================== Testimonials: END ============================================== -->


            </div>
            <!-- /.sidemenu-holder -->
            <!-- ============================================== SIDEBAR : END ============================================== -->

        </div>
        <!-- /.row -->

    </div>
@endsection

@push("js")
    <script>
        $(function () {
            toastr.options
        })
    </script>
@endpush
