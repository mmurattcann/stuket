@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")

@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row single-product'>
                <!-- /.sidebar -->
                <div class='col-md-12'>
                    <div class="detail-block">
                        <div class="row  wow fadeInUp">

                            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">
                                    <div id="owl-single-product">
                                    @foreach($product["images"] as $index => $image)

                                        <div class="single-product-gallery-item" id="slide{{$index+1}}">
                                            <a data-lightbox="image-{{$index+1}}" data-title="Gallery" href="{{getProductImage($image["image"])}}">
                                                <img class="img-responsive" alt="{{$product["title"]}}" src="{{frontAsset("images/blank.gif")}}" data-echo="{{getProductImage($image["image"])}}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                    @endforeach
                                    </div><!-- /.single-product-slider -->
                                    <div class="single-product-gallery-thumbs gallery-thumbs">

                                        <div id="owl-single-product-thumbnails">
                                            @foreach($product["images"] as $index => $image)
                                                <div class="item">
                                                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="{{$index+1}}" href="#slide{{$index+1}}">
                                                        <img class="img-responsive" width="85" alt="{{$product["title"]}}" src="{{getProductImage($image["image"])}}" data-echo="{{getProductImage($image["image"])}}" />
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div><!-- /#owl-single-product-thumbnails -->



                                    </div><!-- /.gallery-thumbs -->

                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->
                            <div class='col-sm-6 col-md-7 product-info-block'>
                                <div class="product-info">
                                    <h1 class="name">{{$product["title"]}}</h1>

                                    <div class="rating-reviews m-t-20">
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-4">
                                                <div class="rating rateit-small"></div>
                                            </div>
                                            <div class="col-sm-8 col-xs-8">
                                                <div class="reviews">
                                                    <a href="#" class="lnk">(13 Reviews)</a>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.rating-reviews -->

                                    <div class="description-container m-t-20">
                                        {!! $product["description"] !!}
                                    </div><!-- /.description-container -->

                                    <div class="price-container info-container m-t-20">
                                        <div class="row">


                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                <div class="price-box">
                                                    <span class="price">{{$product["price"]}}₺</span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                <div class="favorite-button">
                                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Favorilere Ekle" href="#">
                                                        <i class="fa fa-heart"></i>
                                                    </a>
                                                    <a class="btn btn-primary" id="mailButton" data-toggle="tooltip" data-placement="right" title="E-Posta Gönder" href="javascript:void(0);">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            @include("frontend.partials.success-message")

                                        </div><!-- /.row -->
                                    </div><!-- /.price-container -->

                                </div><!-- /.product-info -->
                            </div><!-- /.col-sm-7 -->
                        </div><!-- /.row -->
                    </div>

                    <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                    <li class="active"><a data-toggle="tab" href="#description">İlan Açıklaması</a></li>
                                    <li><a data-toggle="tab" href="#contact">İletişim Bilgileri</a></li>
                                </ul><!-- /.nav-tabs #product-tabs -->
                            </div>
                            <div class="col-sm-9">

                                <div class="tab-content">

                                    <div id="description" class="tab-pane in active">
                                        <div class="product-tab">
                                            <p class="text">{{$product["description"]}}</p>
                                        </div>
                                    </div><!-- /.tab-pane -->

                                    <div id="contact" class="tab-pane">
                                        <div class="product-tab">
                                    <div class="col-md-12">
                                        <h4 class=""><a href="{{route("front.profile", ["userId" => $product["ownerID"]])}}">{{$product["ownerName"]}}</a></h4><br>
                                        <a href="{{route("front.profile", ["userId" => $product["ownerID"]])}}"><img src=" {{$product["ownerImage"]}}" alt="{{$product["ownerName"]}}"></a>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="label-floating"> Telefon Numarası: <a href="tel:{{$product["ownerPhone"]}}">{{$product["ownerPhone"]}} </a> </label>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="label-floating"> Adres:</label> <p>{{$product["address"]}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="label-floating"> E-Posta:</label> <p>{{$product["ownerEmail"]}}</p>
                                        </div>
                                    </div>

                                        </div><!-- /.product-tab -->
                                    </div><!-- /.tab-pane -->

                                </div><!-- /.tab-content -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.product-tabs -->

                    @if(count($similarProducts)>0)
                        <!-- ============================================== UPSELL PRODUCTS ============================================== -->
                        <section class="section featured-product wow fadeInUp">
                            <div class="section-title"><h3>İlginizi Çekebilir</h3></div>
                            <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">

                                @foreach($similarProducts as $similarProduct)
                                    <div class="item item-carousel">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="{{$similarProduct["route"]}}"><img  src="{{$similarProduct["image"]}}" alt="{{$similarProduct["title"]}}"></a>
                                                </div><!-- /.image -->
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="{{$similarProduct["route"]}}"></a>{{$similarProduct["title"]}}</h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
                                                <span class="price">{{$similarProduct["price"]}} ₺</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="detail.html" title="Favorilere Ekle">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->
                                @endforeach
                            </div><!-- /.home-owl-carousel -->
                        </section><!-- /.section -->
                        <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
                    @endif
                </div><!-- /.col -->
                <div class="clearfix"></div>
            </div><!-- /.row -->
    </div><!-- /.body-content -->
        <div class="row">

            @include("frontend.partials.contact-modal", ["userID" => $product["ownerID"]])
        </div>
@endsection

@push("js")
<script>
    $(function () {
        $("#mailButton").click(function () {
            let $modal = $("#contactModal");

            $modal.modal("show");
        })
    })
</script>
@endpush
