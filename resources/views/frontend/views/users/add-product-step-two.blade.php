@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")
<style>
    .ck-editor__editable {
        height: 300px;
    }
</style>
@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div>
            <!-- /.breadcrumb-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                @include("frontend.partials.profile-sidebar")
                <!-- /.sidebar -->
                <div class='col-md-8'>
                <form action="{{route("front.product-complete-steps")}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method("POST")
                    <div class="col-md-12">
                        @include("frontend.partials.error-message")
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-label">Fotoğrafları</label>
                            <input type="file" name="file" class="form-control" multiple>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </div>

@endsection

@push("js")
    <script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '.editor' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
                height: "500px",

            } )
            .catch( error => {
                console.log( error );
            } );

    </script>
    <script>
        $(function () {
            $("#mailButton").click(function () {
                let $modal = $("#contactModal");

                $modal.modal("show");
            })
        })
    </script>

    <script>
        $(function () {

            $('.file-input-area').hide();

            $('input:radio[name="profilePictureRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.file-input-area').show();
                        $('.original-image').hide();

                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.file-input-area').hide();
                        $('.original-image').show();
                        $('input[name="image"]').val("")

                    }
                });
        })
    </script>
@endpush
