@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")
<style>
    .ck-editor__editable {
        height: 300px;
    }
</style>
@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div>
            <!-- /.breadcrumb-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                @include("frontend.partials.profile-sidebar")
                <!-- /.sidebar -->
                <div class='col-md-8'>
                <form action="{{route("front.edit-product-step-two", ["id" => $product["id"]])}}" method="POST">
                    @csrf
                    @method("PUT")
                    <input type="hidden" name="is_active" value="1">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Üniversite</label>
                            <select class="form-control select2 custom-select" id="university_id" name="university_id">

                                <option value="">--- Seçim Yapın ---</option>
                                @foreach($universities as $university)
                                    <option value="{{$university->id}}"
                                            @if($product["university_id"] == $university->id) selected @endif>{{$university->title}}</option>
                                @endforeach
                            </select>
                            @error("university_id")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Fakülte</label>
                            <select class="form-control select2 custom-select" id="faculty_id" name="faculty_id">

                                <option value="">--- Seçim Yapın ---</option>
                                @foreach($faculties as $faculty)
                                    <option value="{{$faculty->id}}"
                                            @if($product["faculty_id"] == $faculty->id) selected @endif>
                                        {{$faculty->title}}</option>
                                @endforeach
                            </select>
                            @error("faculty_id")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Ürün Kategorisi</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if($category->id == $product["categoryID"]) selected @endif>
                                        {{$category->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-label">Ürün Başlığı</label>
                            <input type="text" name="title" class="form-control" value="{{old("title", $product["title"])}}" placeholder="Örn: Öğrenciden Telefon">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-label">Ürün Fiyatı(₺)</label>
                            <input type="number" name="price" value="{{old("price", $product["price"])}}" class="form-control" placeholder="Örn: 1385">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label class="form-label">Ürün Açıklaması</label>
                            <textarea  name="description"  rows="6" class="form-control editor">{{old("description", $product["description"])}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label class="form-label">Adres</label>
                            <textarea  name="address"  rows="6" class="form-control">{{old("address", $product["address"])}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">Sonraki Adım (Ürün Görselleri)  <i class="fa fa-arrow-right"></i></button>
                    </div>
                </form>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </div>

@endsection

@push("js")
    <script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '.editor' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
                height: "500px",

            } )
            .catch( error => {
                console.log( error );
            } );

    </script>
    <script>
        $(function () {
            $("#mailButton").click(function () {
                let $modal = $("#contactModal");

                $modal.modal("show");
            })
        })
    </script>

    <script>
        $(function () {

            $('.file-input-area').hide();

            $('input:radio[name="profilePictureRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.file-input-area').show();
                        $('.original-image').hide();

                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.file-input-area').hide();
                        $('.original-image').show();
                        $('input[name="image"]').val("")

                    }
                });
        })
    </script>
@endpush
