@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")

    <!---Sweetalert Css-->
    <link href="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/sweet-alert/sweetalert.css")}}" rel="stylesheet" />
@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div>
            <!-- /.breadcrumb-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                @include("frontend.partials.profile-sidebar")
                <!-- /.sidebar -->
                <div class='col-md-8'>
                    <!-- ========================================== SECTION – HERO ========================================= -->

                    <div id="category" class="category-carousel hidden-xs">
                        <div class="item">
                            <div class="image"> <img src="{{frontAsset('images/banners/e-commerce.jpg')}}" alt="Kategori Banner" class="img-responsive"> </div>
                            <div class="container-fluid">
                                <div class="caption vertical-top text-left">
                                    <div class="big-text">{{$user->name}}</div>
                                    <div class="excerpt hidden-sm hidden-md">Kullacısına ait ürünler </div>
                                    <div class="excerpt-normal hidden-sm hidden-md"> <strong>Toplam {{count($products) }} Sonuç Bulundu</strong> </div>
                                </div>
                                <!-- /.caption -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                    </div>


                    <div class="clearfix filters-container m-t-10">
                        <div class="row">
                            <div class="col col-sm-6 col-md-3 col-lg-2 col-xs-6">
                                <div class="filter-tabs">
                                    <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                                        <li class="active"> <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-large"></i>Ürünler</a> </li>
                                    </ul>
                                </div>
                                <!-- /.filter-tabs -->
                            </div>
                            <!-- /.col -->
                            <div class="col col-sm-12 col-md-5 col-lg-6 hidden-xs hidden-sm" style="visibility: hidden">
                                <div class="col col-sm-6 col-md-12 col-lg-6 no-padding">
                                    <div class="lbl-cnt"> <span class="lbl">Sırala</span>
                                        <div class="fld inline">
                                            <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                                <button data-toggle="dropdown" type="button" class="btn dropdown-toggle"> Seç <span class="caret"></span> </button>
                                                <ul role="menu" class="dropdown-menu">
                                                    <li role="presentation"><a href="#">Fiyat: Önce En Düşük</a></li>
                                                    <li role="presentation"><a href="#">Fiyat: Önce En Yüksek</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.fld -->
                                    </div>
                                    <!-- /.lbl-cnt -->
                                </div>
                                <!-- /.col -->
                                <!-- /.col -->
                            </div>

                            <!-- /.col -->
                            <div class="col col-sm-6 col-md-4 col-xs-6 col-lg-4 text-right" style="visibility: hidden">
                                <div class="pagination-container">
                                    <ul class="list-inline list-unstyled">
                                        <li class="prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li class="active"><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li class="next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                    </ul>
                                    <!-- /.list-inline -->
                                </div>
                                <!-- /.pagination-container --> </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="search-result-container ">
                        <div id="myTabContent" class="tab-content category-list">
                            <div class="tab-pane active " id="grid-container">
                                <div class="category-product">
                                    <div class="row">
                                        @foreach($products as $product)
                                            <div class="col-sm-6 col-md-6 wow fadeInUp" id="product-box-{{$product["id"]}}">
                                                <div class="products">
                                                    <div class="product">
                                                        <div class="product-image">
                                                            <div class="image"> <a href="{{$product["route"]}}"><img  src="{{$product["image"]}}" alt="{{$product["title"]}}"></a> </div>
                                                            <!-- /.image -->

                                                            <div class="tag new"><span>{{$product["categoryTitle"]}}</span></div>
                                                        </div>
                                                        <!-- /.product-image -->

                                                        <div class="product-info text-left">
                                                            <h3 class="name"><a href="{{$product["route"]}}">{{$product["title"]}}</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="description"></div>
                                                            <div class="product-price"> <span class="price"> {{$product["price"]}} ₺ </span> </div>
                                                            @auth
                                                                @if(Auth::user()->id == $product["ownerID"])
                                                                    <div>
                                                                        <a href="{{route("front.edit-product", ["id" => $product["id"]])}}"
                                                                           class="btn btn-success">
                                                                            <i class="fa fa-edit"></i>
                                                                        </a>
                                                                        <button
                                                                            type="button"
                                                                            class="btn btn-danger delete-button"
                                                                            data-id="{{$product["id"]}}"
                                                                            data-route="{{route("products.destroy", $product["id"])}}" >

                                                                            <i class="fa fa-trash"></i>
                                                                        </button>
                                                                    </div>
                                                                @endif
                                                            @endauth
                                                            <!-- /.product-price -->

                                                        </div>
                                                        <!-- /.product-info -->
                                                        <div class="cart clearfix animate-effect">
                                                            <div class="action">
                                                                <ul class="list-unstyled">
                                                                    <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html" title="Favorilere Ekle"> <i class="icon fa fa-heart"></i> </a> </li>
                                                                </ul>
                                                            </div>
                                                            <!-- /.action -->
                                                        </div>
                                                        <!-- /.cart -->
                                                    </div>
                                                    <!-- /.product -->

                                                </div>
                                                <!-- /.products -->
                                            </div>
                                            <!-- /.item -->
                                        @endforeach
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.category-product -->

                            </div>
                            <!-- /.tab-pane -->

                            <!-- /.tab-pane #list-container -->
                        </div>
                        <!-- /.tab-content -->
                        <div class="clearfix filters-container">
                            <div class="text-right">
                                <div class="pagination-container">

                                    <!-- /.list-inline -->
                                </div>
                                <!-- /.pagination-container --> </div>
                            <!-- /.text-right -->

                        </div>
                        <!-- /.filters-container -->

                    </div>
                    <!-- /.search-result-container -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        @include("frontend.partials.contact-modal", ["userID" => $user->id])
    </div>

@endsection

@push("js")
    <!-- Sweet alert Plugin -->
    <script src="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.js")}}"></script>
    <script src="{{panelAsset("plugins/sweet-alert/sweetalert.min.js")}}"></script>
    <script src="{{panelAsset("js/sweet-alert.js")}}"></script>
    <script>
        $(function () {
            $(".delete-button").click( function () {

                var id = $(this).data("id");
                var $productBox = $("#product-box-"+id);
                var $route = $(this).data("route");
                swal({
                        title: 'Silmek istediğinize emin misiniz?',
                        text: "Bilgiler kalıcı olarak silinecek!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Evet, sil!',
                        cancelButtonText: 'Vazgeç!',
                        reverseButtons: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            var token = $("meta[name='csrf-token']").attr("content");
                            $.ajax({
                                url: $route,
                                type: 'DELETE',
                                data: {
                                    "id": id,
                                    "_token": token,
                                },
                                success: function (data) {
                                    $productBox.fadeOut(1000);

                                    toastr.options.closeButton = false;
                                    toastr.options.preventDuplicates = false;
                                    toastr.options.closeMethod = 'fadeOut';
                                    toastr.options.closeDuration = 100;
                                    toastr.success(data.message);
                                }
                            });
                        }
                    }
                )
            })
        })
    </script>
    <script>
        $(function () {
            $("#mailButton").click(function () {
                let $modal = $("#contactModal");

                $modal.modal("show");
            })
        })
    </script>

    <script>
        $(function () {

            $('.file-input-area').hide();

            $('input:radio[name="profilePictureRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.file-input-area').show();
                        $('.original-image').hide();

                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.file-input-area').hide();
                        $('.original-image').show();
                        $('input[name="image"]').val("")

                    }
                });
        })
    </script>
@endpush
