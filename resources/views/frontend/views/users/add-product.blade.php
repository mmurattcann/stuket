@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")
<style>
    .ck-editor__editable {
        height: 300px;
    }
</style>
@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div>
            <!-- /.breadcrumb-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                @include("frontend.partials.profile-sidebar")
                <!-- /.sidebar -->
                <div class='col-md-8'>
                <form action="{{route("front.product-step-two")}}" method="POST">
                    @csrf
                    @method("POST")
                    <input type="hidden" name="is_active" value="1">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Üniversite</label>
                            <select name="university_id" class="form-control">
                                @foreach($universities as $university)
                                    <option value="{{$university->id}}">{{$university->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Fakülte</label>
                            <select name="university_id" class="form-control">
                                @foreach($faculties as $faculty)
                                    <option value="{{$faculty->id}}">{{$faculty->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Ürün Kategorisi</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-label">Ürün Başlığı</label>
                            <input type="text" name="title" class="form-control" value="{{old("title")}}" placeholder="Örn: Öğrenciden Telefon">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-label">Ürün Fiyatı(₺)</label>
                            <input type="number" name="price" value="{{old("price")}}" class="form-control" placeholder="Örn: 1385">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label class="form-label">Ürün Açıklaması</label>
                            <textarea  name="description"  rows="6" class="form-control editor">{{old("description")}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label class="form-label">Adres</label>
                            <textarea  name="address"  rows="6" class="form-control">{{old("address")}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">Sonraki Adım (Ürün Görselleri)  <i class="fa fa-arrow-right"></i></button>
                    </div>
                </form>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </div>

@endsection

@push("js")
    <script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '.editor' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
                height: "500px",

            } )
            .catch( error => {
                console.log( error );
            } );

    </script>
    <script>
        $(function () {
            $("#mailButton").click(function () {
                let $modal = $("#contactModal");

                $modal.modal("show");
            })
        })
    </script>

    <script>
        $(function () {

            $('.file-input-area').hide();

            $('input:radio[name="profilePictureRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.file-input-area').show();
                        $('.original-image').hide();

                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.file-input-area').hide();
                        $('.original-image').show();
                        $('input[name="image"]').val("")

                    }
                });
        })
    </script>
@endpush
