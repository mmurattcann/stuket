@extends("layouts.frontend._layout")

@section("title")
    {{$title}}
@endsection

@push("css")

<!-- DROPZone -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">

<!---Sweetalert Css-->
<link href="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.css")}}" rel="stylesheet" />
<link href="{{panelAsset("plugins/sweet-alert/sweetalert.css")}}" rel="stylesheet" />
@endpush

@section("content")
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Anasayfa</a></li>
                    <li class='active'>{{$title}}</li>
                </ul>
            </div>
            <!-- /.breadcrumb-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                @include("frontend.partials.profile-sidebar")
                <!-- /.sidebar -->
                <div class='col-md-8'>
                    <div class="col-md-12">
                        @include("frontend.partials.error-message")
                    </div>


                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Yeni Fotoğraf Ekle
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="col-md-12">

                                            <form action="{{route("front.update-product", $product->id)}}" class="dropzone" id="dropzone" name="image-form" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method("POST")

                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <div class="dz-message">
                                                    <div class="col-xs-8">
                                                        <div class="message">
                                                            <h4>Yüklemek istediğiniz dosyaları buraya sürükleyip bırakın</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple />
                                                </div>
                                                <div class="col-md-12">
                                                    <button type="button" id="upload-button" class="btn btn-primary pull-right mt-3"><i class="fa fa-upload"></i>  Yükle</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Mevcut Ürün Fotoğrafları
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <table id="front-image-table" class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Görsel</th>
                                                <th>Sil</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($images as $image)
                                                <tr id="tr-{{$image->id}}">
                                                    <td><img src="{{getProductImage($image->image)}}"
                                                             width="180"
                                                             height="150"></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger delete-button"
                                                                data-id="{{$image->id}}"
                                                                data-route="{{route("front.delete-product-image", $product->id)}}">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table> </div>
                                </div>
                            </div>

                            </div>
                        </div>

                    </div>


                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </div>

@endsection

@push("js")

    <script src="{{panelAsset("js/dropzone.js")}}"></script>

    <!-- User CRUD Operations-->
    <script src="{{panelAsset("CustomOperations/productCategoryCrud.js")}}"></script>
    <script>
        $(function () {

            $('.file-input-area').hide();

            $('input:radio[name="profilePictureRadio"]').change(
                function(){
                    if ($(this).is(':checked') && $(this).val() == 2) {

                        $('.file-input-area').show();
                        $('.original-image').hide();

                    }else  if ($(this).is(':checked') && $(this).val() == 1) {

                        $('.file-input-area').hide();
                        $('.original-image').show();
                        $('input[name="image"]').val("")

                    }
                });
        })
    </script>
    <script>
        Dropzone.autoDiscover=false;
        Dropzone.autoDiscover=false;

        $(document).ready(function () {

            let myDropzone = new Dropzone("#dropzone", {
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                parallelUploads: 20,
                addRemoveLinks: true,
                autoProcessQueue: false,
                success: function (file, response) {
                    console.log("dosyalar eklendi");
                    /* $.each(response.image, function(k, v) {

                    var html = "<tr>";

                        html += '<td>'+v.id + '</td>';
                        html += '<td><img src=' + imageURL + '></td>' ;
                        html += '<td>'+"RESİM"+'<td>';
                            html += '<td>'+v.created_at + '</td>';
                        html += '<td><a href="javascript:void(0)" class="btn btn-danger btn-lg" id="image-delete-button" data-id="'+v.id+'">'+ ' <i class="fa fa-trash"></i>'+ '</a></td><tr>';

                        $("#table-body").append(html);
                        });
                        */
                },

            });

            $('#upload-button').click(function () {
                myDropzone.processQueue();
            })

            $("#saveProduct").click(function () {
                $("#product-form").submit();
            });
        });

    </script>

    <!-- Sweet alert Plugin -->
    <script src="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.js")}}"></script>
    <script src="{{panelAsset("plugins/sweet-alert/sweetalert.min.js")}}"></script>
    <script src="{{panelAsset("js/sweet-alert.js")}}"></script>
    <script>
        $(function () {
            $(".delete-button").click( function () {

                var id = $(this).data("id");
                var $tr = $("#tr-"+id);
                var $route = $(this).data("route");
                swal({
                        title: 'Silmek istediğinize emin misiniz?',
                        text: "Bilgiler kalıcı olarak silinecek!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Evet, sil!',
                        cancelButtonText: 'Vazgeç!',
                        reverseButtons: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            var token = $("meta[name='csrf-token']").attr("content");
                            $.ajax({
                                url: $route,
                                type: 'DELETE',
                                data: {
                                    "id": id,
                                    "_token": token,
                                },
                                success: function (data) {
                                    $productBox.fadeOut(1000);

                                    toastr.options.closeButton = false;
                                    toastr.options.preventDuplicates = false;
                                    toastr.options.closeMethod = 'fadeOut';
                                    toastr.options.closeDuration = 100;
                                    toastr.success(data.message);
                                }
                            });
                        }
                    }
                )
            })
        })
    </script>
@endpush
