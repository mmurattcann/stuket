
<div class="header-nav animate-dropdown">
    <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                    <div class="nav-outer">
                        <ul class="nav navbar-nav">
                            <li class=""> <a href="{{route("front.index")}}" >Anasayfa</a> </li>
                            <li class=""> <a href="{{route("front.product-list")}}" >Tüm Ürünler</a> </li>

                        @foreach($productCategories as $menu)
                                <li class="dropdown yamm mega-menu">
                                    <a href="{{route("front.category", $menu->slug)}}" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                                        {{$menu->title}}
                                        @if($menu->childs->count() > 0)<i class="fa fa-angle-down"></i> @endif</a>
                                @if($menu->childs->count() > 0)
                                   <ul class="dropdown-menu container">
                                            <li>
                                                <div class="yamm-content">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-2 col-menu">
                                                            <ul class="links">
                                                                <li>
                                                                    <b style="text-decoration: underline">
                                                                        <a href="{{route("front.category", $menu->slug)}}">Tüm {{$menu->title}} Ürünleri</a>
                                                                    </b>
                                                                </li>
                                                                @foreach($menu->childs as $child)
                                                                    <li><a href="{{route("front.category", $child->slug)}}">{{$child->title}}</a></li>
                                                                @endforeach

                                                            </ul>
                                                        </div>
                                                        <!-- /.col -->


                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-menu custom-banner"> <a href="#"><img alt="" src="assets/images/banners/banner-side.png"></a> </div>
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.yamm-content --> </li>
                                        </ul>
                                    </li>

                                @else
                                @endif
                            @endforeach
                            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Blog <i class="fa fa-angle-down"></i> </a>
                                <ul class="dropdown-menu pages">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-xs-12 col-menu">
                                                    <ul class="links">
                                                        <li><a href="{{route("front.get-all-blog")}}" style="text-decoration: underline;"><b>Tüm Kategoriler </b> </a></li>
                                                        @foreach($blogCategories as $blogCategory)
                                                            <li><a href="#">{{$blogCategory->title}}</a></li>
                                                        @endforeach
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            @auth()
                            <li class="dropdown pull-right"> <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    <img src="{{getProfileImage(Auth::user()->image)}}" class="img-circle" width="25" height="25"> <span class="ml-2">{{Auth::user()->name}} </span><i class="fa fa-angle-down"></i> </a>
                                <ul class="dropdown-menu pages">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-xs-12 col-menu">
                                                    <ul class="links">
                                                        <li>
                                                            <a href="{{route("front.profile", ["userId" => Auth::user()->id])}}"
                                                               style="text-decoration: none;">
                                                                <b>
                                                                    <i class="fa fa-user"></i> Profil
                                                                </b>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route("front.create-product")}}"
                                                               style="text-decoration: none;">
                                                                <b>
                                                                    <i class="fa fa-cubes"></i> Ürün Ekle
                                                                </b>
                                                            </a>
                                                        </li>
                                                       <li>
                                                            <a href="{{route("front.logout")}}"
                                                               style="text-decoration: none;">
                                                                <b>
                                                                    <i class="fa fa-power-off"></i> Çıkış Yap
                                                                </b>
                                                            </a>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            @else
                                <li class="active pull-right"> <a href="{{route("front.authentication")}}" ><i class="fa fa-user"></i>&nbsp;&nbsp;Giriş Yap / Kaydol</a> </li>
                            @endauth

                        </ul>
                        <!-- /.navbar-nav -->
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.nav-outer -->
                </div>
                <!-- /.navbar-collapse -->

            </div>
            <!-- /.nav-bg-class -->
        </div>
        <!-- /.navbar-default -->
    </div>
    <!-- /.container-class -->

</div>
