<div class="top-bar animate-dropdown">
    <div class="container">
        <div class="header-top-inner">
            <div class="cnt-account">
                <ul class="list-unstyled">
                    <li><a href="{{route("front.contact-page")}}"><i class="icon fa fa-envelope"></i>İletişim</a></li>
                    <li class="hidden-xs"><a href="{{route("front.authentication")}}"><i class="icon fa fa-check"></i>Giriş Yap</a></li>
                    <li><a href="{{route("front.authentication")}}"><i class="icon fa fa-lock"></i>Kaydol</a></li>
                </ul>
            </div>
            <!-- /.cnt-account -->

            <div class="clearfix"></div>
        </div>
        <!-- /.header-top-inner -->
    </div>
    <!-- /.container -->
</div>
