<div class="sidebar-widget outer-bottom-xs wow fadeInUp">
    <h3 class="section-title">Kategoriler</h3>
    <div class="sidebar-widget-body m-t-10">
        <ul>
            @foreach($blogCategories as $category)

                <li><a href="{{route("front.get-blogs-by-category", $category->slug)}}"> <h4>{{$category->title}}</h4></a></li>
            @endforeach
        </ul>
    </div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->
