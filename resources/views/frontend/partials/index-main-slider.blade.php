<div id="hero">
    <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
        @foreach($indexSliders as $indexSlider)
            <div class="item" style="background-image: url('{{$indexSlider["image"]}}');">
            <div class="container">
                <div class="caption bg-color vertical-center text-left">
                    <div class="slider-header fadeInDown-1">{{$indexSlider["top_title"]}}</div>
                    <div class="big-text fadeInDown-1">{{$indexSlider["title"]}} </div>
                    <div class="excerpt fadeInDown-2 hidden-xs"> <span>{{$indexSlider["description"]}}</span> </div>
                    @if($indexSlider["has_button"])
                        <div class="button-holder fadeInDown-3">
                            <a href="{{$indexSlider["buttonUrl"]}}"
                               target="_blank"
                               class="btn-lg btn btn-uppercase {{$indexSlider["button_class"]}}">
                                {{$indexSlider["button_title"]}}
                            </a>
                        </div>
                    @endif
                </div>
                <!-- /.caption -->
            </div>
            <!-- /.container-fluid -->
        </div>
        @endforeach
        <!-- /.item -->
    </div>
    <!-- /.owl-carousel -->
</div>
