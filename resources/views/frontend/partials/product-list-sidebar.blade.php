<div class='col-md-3 sidebar'>
    <!-- ================================== TOP NAVIGATION ================================== -->
    <!-- /.side-menu -->
    <!-- ================================== TOP NAVIGATION : END ================================== -->
    <div class="sidebar-module-container">
        <div class="sidebar-filter">
            <form action="{{route("front.search-product")}}" method="POST">
            @csrf
            @method("POST")
                <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                <div class="sidebar-widget wow fadeInUp">
                    <h3 class="section-title">Filtrele</h3>
                    <div class="widget-header">

                    </div>

                    <div class="sidebar-widget-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                   <input type="text" name="query" class="form-control" placeholder="Aradığınız İlan Başlığı">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-widget-body">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Üniversite</label>
                                        <select class="form-control select2 custom-select" id="university_id" name="university_id">

                                            <option value="">--- Seçim Yapın ---</option>
                                            @foreach($universities as $university)
                                                <option value="{{$university->id}}">{{$university->title}}</option>
                                            @endforeach
                                        </select>
                                        @error("university_id")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Fakülte</label>
                                        <select class="form-control select2 custom-select" id="university_id" name="faculty_id">

                                            <option value="">--- Seçim Yapın ---</option>
                                            @foreach($faculties as $faculty)
                                                <option value="{{$faculty->id}}">{{$faculty->title}}</option>
                                            @endforeach
                                        </select>
                                        @error("faculty_id")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="sidebar-widget-body">
                        <div class="row">
                        @foreach($categories as $category)
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="category_id" id="category_id" value="{{$category["id"]}}">
                                    <label class="form-check-label" for="category_id">
                                        {{$category["title"]}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                    <!-- /.sidebar-widget-body -->
                </div>
                <!-- /.sidebar-widget -->
                <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->

                <!-- ============================================== PRICE SILDER============================================== -->
                <div class="sidebar-widget wow fadeInUp">
                <div class="widget-header">
                    <h4 class="widget-title">Fiyat Aralığı</h4>
                </div>
                <div class="sidebar-widget-body m-t-10">
                   <div class="form-group col-md-12">
                       <input type="number" name="lowest_price" class="form-control" placeholder="En Düşük Fiyat(₺)">
                   </div>
                   <div class="form-group col-md-12">
                       <input type="number" name="highest_price" class="form-control" placeholder="En Yüksek Fiyat(₺)">
                   </div>
                    <!-- /.price-range-holder -->
                    <button type="submit" class="lnk btn btn-primary" > Ara</button> </div>
                <!-- /.sidebar-widget-body -->
            </div>
            </form>
            <!-- /.sidebar-widget -->
        </div>
        <!-- ============================================== COMPARE: END ============================================== -->
        <!-- ============================================== PRODUCT TAGS ============================================== -->
        <!----------- Testimonials------------->
        <div class="sidebar-widget testimonials-block outer-top-vs ">
            <div id="advertisement" class="advertisement">
                <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member1.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">John Doe <span>Abc Company</span> </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.item -->

                <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member3.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">Stephen Doe <span>Xperia Designs</span> </div>
                </div>
                <!-- /.item -->

                <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member2.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">Saraha Smith <span>Datsun &amp; Co</span> </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.item -->

            </div>
            <!-- /.owl-carousel -->
        </div>

        <!-- ============================================== Testimonials: END ============================================== -->


    </div>
    <!-- /.sidebar-filter -->

    <!-- /.sidebar-module-container -->
</div>
