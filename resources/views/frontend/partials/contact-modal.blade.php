<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contactModalLongTitle">Satıcıya Mail Atıyorsunuz</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route("front.send-mail-to-seller")}}" method="POST">
                @csrf
                @method("POST")
                <input type="hidden" name="user_id" value="{{$userID}}">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">


                            <div class="col-md-6 form-group">
                                <label class="form-label">Ad Soyad</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="form-label">E-Posta</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="form-label">Telefon</label>
                                <input type="text" class="form-control" name="phone" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="form-label">Konu</label>
                                <input type="text" class="form-control" name="subject" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="form-label">Mesaj</label>
                                <textarea class="form-control" name="message" required rows="5"></textarea>
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Vazgeç</button>
                <button type="submit" class="btn btn-info">Gönder</button>
            </div>
            </form>
        </div>
    </div>
</div>
