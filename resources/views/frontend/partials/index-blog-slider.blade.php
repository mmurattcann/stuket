<section class="section latest-blog outer-bottom-vs">
    <div class="section-title"><h3>Son Blog Yazıları</h3></div>
    <div class="blog-slider-container outer-top-xs">
        <div class="owl-carousel blog-slider custom-carousel">
            @foreach($indexBlogs as  $indexBlog)
                <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <a href="{{$indexBlog["route"]}}">
                                <img src="{{$indexBlog["image"]}}" alt="{{$indexBlog["title"]}}">
                            </a>
                        </div>
                    </div>
                    <!-- /.blog-post-image -->

                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="{{$indexBlog["route"]}}">{{$indexBlog["title"]}}</a></h3>
                        <span class="info">{{$indexBlog["user"]}} &nbsp;|&nbsp; {{$indexBlog["created_at"]}}</span>
                        <p class="text">{{$indexBlog["short_description"]}}</p>
                        <a href="{{$indexBlog["route"]}}" class="lnk btn btn-primary">Okumaya Devam Et</a> </div>
                    <!-- /.blog-post-info -->

                </div>
                <!-- /.blog-post -->
            </div>
            @endforeach
            <!-- /.item -->
        </div>
        <!-- /.owl-carousel -->
    </div>
    <!-- /.blog-slider-container -->
</section>
