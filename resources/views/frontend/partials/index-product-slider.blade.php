<section class="section featured-product wow fadeInUp">
    <div class="section-title"><h3>Son Ürünler</h3> <a href="{{route("front.product-list")}}">Tüm Ürünleri Görüntüle</a></div>
    <div class="featured-block">
        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
            @foreach($indexProducts as $indexProduct)
                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image"> <a href="{{$indexProduct["route"]}}"><img  src="{{$indexProduct["image"]}}" alt="{{$indexProduct["title"]}}"></a> </div>
                                <!-- /.image -->
                                <div class="tag hot"><span>Yeni</span></div>
                            </div>
                            <!-- /.product-image -->

                            <div class="product-info text-left">
                                <h3 class="name"><a href="{{$indexProduct["route"]}}">{{$indexProduct["title"]}}</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price"> <span class="price"> {{$indexProduct["price"]}} ₺</span> <span class="price-before-discount">$800</span> </div>
                                <!-- /.product-price -->

                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                        <li class="lnk"> <a class="add-to-cart" href="detail.html" title="Compare"> <i class="fa fa-signal" aria-hidden="true"></i> </a> </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->

                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->
            @endforeach
        </div>
    </div>
    <!-- /.home-owl-carousel -->
</section>
