<div class='col-md-3 sidebar'>
    <!-- ================================== TOP NAVIGATION ================================== -->
    <!-- /.side-menu -->
    <!-- ================================== TOP NAVIGATION : END ================================== -->
    <div class="sidebar-module-container">
        <div class="sidebar-filter">
                <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                <div class="sidebar-widget wow fadeInUp">
                    <h3 class="section-title text-center">{{$user->name}}</h3>

                    <div class="sidebar-widget-body">
                        <div class="row">
                            @if(Auth::check() && Auth::user()->id == $user->id )
                                <form action="{{route("front.update-user", ["id" => $user->id])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method("PUT")
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Profil Resmi</label><br>
                                        <img src="{{getProfileImage($user->image)}}" class="original-image" alt="{{$user->name}}">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" name="profilePictureRadio" value="1" checked>
                                            <span class="custom-control-label">Orijinali Koru</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" name="profilePictureRadio" value="2">
                                            <span class="custom-control-label">Değiştir</span>
                                        </label>
                                    </div>
                                    <div class="form-group file-input-area">

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Ad Soyad</label>
                                        <input type="text" name="name" value="{{old("name", $user->name)}}" class="form-control">
                                        @error("name")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">E-Posta</label>
                                        <input type="text" name="email" value="{{old("email", $user->email)}}" class="form-control">
                                        @error("email")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Telefon</label>
                                        <input type="text" name="phone" value="{{old("phone", $user->phone)}}" class="form-control">
                                        @error("phone")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Şifre</label>
                                        <input type="password" name="password"  class="form-control">
                                        @error("password")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Şifre(Tekrar)</label>
                                        <input type="password" name="password_confirmation"  class="form-control">
                                        @error("password_confirmation")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="form-label">Hakkında</label>
                                        <textarea name="about"  class="form-control">{{old("about", $user->about)}}</textarea>
                                        @error("about")
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Güncelle</button>
                                    </div>
                                </form>
                            @else
                                <div class="form-group col-md-12">
                                    <div class="col-md-12 text-center">
                                        <img src="{{getProfileImage($user->image)}}" alt="{{$user->name}}"><br><br>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Telefon: </label>
                                        <a href="tel:{{$user->phone}}" class="text-primary">{{$user->phone}}</a>
                                    </div>
                                    <div class="col-md-12">
                                        <label>E-Posta: </label>
                                        <a href="mailto:{{$user->email}}" class="text-primary">{{$user->email}}</a>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="form-label">Hakkında: </label>
                                        <p>{!! $user->about !!}</p>
                                    </div>
                                    <div class="col-md-12">
                                        <a class="btn btn-primary" id="mailButton" data-toggle="tooltip" data-placement="right" title="E-Posta Gönder" href="javascript:void(0);">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>
                    <!-- /.sidebar-widget-body -->
                </div>
                <!-- /.sidebar-widget -->
                <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->

                <!-- ============================================== PRICE SILDER============================================== -->
            <!-- /.sidebar-widget -->
        </div>
        <!-- ============================================== COMPARE: END ============================================== -->
        <!-- ============================================== PRODUCT TAGS ============================================== -->
        <!----------- Testimonials------------->
        <div class="sidebar-widget testimonials-block outer-top-vs ">
            <div id="advertisement" class="advertisement">
                <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member1.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">John Doe <span>Abc Company</span> </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.item -->

                <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member3.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">Stephen Doe <span>Xperia Designs</span> </div>
                </div>
                <!-- /.item -->

                <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member2.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">Saraha Smith <span>Datsun &amp; Co</span> </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.item -->

            </div>
            <!-- /.owl-carousel -->
        </div>

        <!-- ============================================== Testimonials: END ============================================== -->


    </div>
    <!-- /.sidebar-filter -->

    <!-- /.sidebar-module-container -->
</div>
