@extends('layouts.panel._authentication_layout')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col mx-auto">
                <div class="text-center mb-6">
                    <img src="{{panelAsset("images/brand/logo.png")}}" class="" alt="">
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card-group mb-0">
                            <div class="card p-4">
                                <div class="card-body">
                                    <h1>Giriş Yap</h1>
                                    <p class="text-muted">Forma bilgilerinizi girin</p>
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf


                                        <div class="input-group mb-3">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="E-Posta" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="input-group mb-4">

                                            <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Şifre" required autocomplete="current-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror

                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-gradient-primary btn-block">Giriş Yap</button>
                                        </div>
                                    </form>
                                    <div class="row">

                                        @if (Route::has('password.request'))
                                            <div class="col-12">
                                                <a href="{{ route('password.request') }}" class="btn btn-link box-shadow-0 px-0">Şifremi Unuttum</a>
                                            </div>
                                        @endif

                                    </div>
                                </div>

                            </div>
                            <div class="card text-white bg-primary py-5 d-md-down-none login-transparent">
                                <div class="card-body text-center justify-content-center ">
                                    <h2>Giriş</h2>
                                    <p>Formda bulunan alanları doldurarak giriş yapabilir, panele ait özellikleri kullanmaya başlayabilirsiniz.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
