<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themesground.com/marazzo-demo/V3/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Feb 2020 20:37:32 GMT -->
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield("title") - {{$siteTitle}}</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{frontAsset("css/bootstrap.min.css")}}">

    <!-- Customizable CSS -->
    <link rel="stylesheet" href="{{frontAsset("css/main.css")}}">
    <link rel="stylesheet" href="{{frontAsset("css/blue.css")}}">
    <link rel="stylesheet" href="{{frontAsset("css/owl.carousel.css")}}">
    <link rel="stylesheet" href="{{frontAsset("css/owl.transitions.css")}}">
    <link rel="stylesheet" href="{{frontAsset("css/animate.min.css")}}">
    <link rel="stylesheet" href="{{frontAsset("css/rateit.css")}}">
    <link rel="stylesheet" href="{{frontAsset("css/bootstrap-select.min.css")}}">

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Mouse+Memoirs" rel="stylesheet">

    <!-- Toastr CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    @stack("css")
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

    <!-- ============================================== TOP MENU ============================================== -->
   @include("frontend.includes.top-header")
    <!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
   @include("frontend.includes.bottom-header")
    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    @include("frontend.includes.menu")
    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

</header>

<!-- ============================================== HEADER : END ============================================== -->

    @yield("content")
    <!-- /.container -->

<!-- /#top-banner-and-menu -->



<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
    <div class="newsletter-row">
        <div class="container">
            <div class="row">

                <!-- Footer Newsletter -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col1">
                    <div class="newsletter-wrap">
                        <h5>Newsletter</h5>
                        <h4>Get discount 30% off</h4>
                        <form action="#" method="post" id="newsletter-validate-detail1">
                            <div id="container_form_news">
                                <div id="container_form_news2">
                                    <input type="text" name="email" id="newsletter1" title="Sign up for our newsletter" class="input-text required-entry validate-email" placeholder="Enter your email address">
                                    <button type="submit" title="Subscribe" class="button subscribe"><span>Subscribe</span></button>
                                </div>
                                <!--container_form_news2-->
                            </div>
                            <!--container_form_news-->
                        </form>
                    </div>
                    <!--newsletter-wrap-->
                </div>
            </div>
        </div>
        <!--footer-column-last-->
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="module-heading">
                        <h4 class="module-title">İletişim</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class="toggle-footer" style="">
                            <li class="media">
                                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span> </div>
                                <div class="media-body">
                                    <p>{{$siteAddress}}</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
                                <div class="media-body">
                                    <p>{{$phoneNumber}}<br>
                                       {{$mobilePhoneNumber}}</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
                                <div class="media-body"> <span><a href="#">{{$infoEmail}}</a></span> </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

              {{--  <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Customer Service</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="#" title="Contact us">My Account</a></li>
                            <li><a href="#" title="About us">Order History</a></li>
                            <li><a href="#" title="faq">FAQ</a></li>
                            <li><a href="#" title="Popular Searches">Specials</a></li>
                            <li class="last"><a href="#" title="Where is my order?">Help Center</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Corporation</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a title="Your Account" href="#">About us</a></li>
                            <li><a title="Information" href="#">Customer Service</a></li>
                            <li><a title="Addresses" href="#">Company</a></li>
                            <li><a title="Addresses" href="#">Investor Relations</a></li>
                            <li class="last"><a title="Orders History" href="#">Advanced Search</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Why Choose Us</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="#" title="About us">Shopping Guide</a></li>
                            <li><a href="#" title="Blog">Blog</a></li>
                            <li><a href="#" title="Company">Company</a></li>
                            <li><a href="#" title="Investor Relations">Investor Relations</a></li>
                            <li class=" last"><a href="contact-us.html" title="Suppliers">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>--}}
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                    <li class="fb pull-left"><a target="_blank" rel="nofollow" href="#" title="Facebook"></a></li>
                    <li class="tw pull-left"><a target="_blank" rel="nofollow" href="#" title="Twitter"></a></li>
                    <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="#" title="GooglePlus"></a></li>
                    <li class="rss pull-left"><a target="_blank" rel="nofollow" href="#" title="RSS"></a></li>
                    <li class="pintrest pull-left"><a target="_blank" rel="nofollow" href="#" title="PInterest"></a></li>
                    <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="#" title="Linkedin"></a></li>
                    <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="#" title="Youtube"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
                        <li><img src="assets/images/payments/1.png" alt=""></li>
                        <li><img src="assets/images/payments/2.png" alt=""></li>
                        <li><img src="assets/images/payments/3.png" alt=""></li>
                        <li><img src="assets/images/payments/4.png" alt=""></li>
                        <li><img src="assets/images/payments/5.png" alt=""></li>
                    </ul>
                </div>
                <!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= -->

<!-- For demo purposes – can be removed on production -->

<!-- For demo purposes – can be removed on production : End -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{frontAsset('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{frontAsset('js/bootstrap.min.js')}}"></script>
<script src="{{frontAsset('js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{frontAsset('js/owl.carousel.min.js')}}"></script>
<script src="{{frontAsset('js/echo.min.js')}}"></script>
<script src="{{frontAsset('js/jquery.easing-1.3.min.js')}}"></script>
<script src="{{frontAsset('js/bootstrap-slider.min.js')}}"></script>
<script src="{{frontAsset('js/jquery.rateit.min.js')}}"></script>
<script type="text/javascript" src="{{frontAsset('js/lightbox.min.js')}}"></script>
<script src="{{frontAsset('js/bootstrap-select.min.js')}}"></script>
<script src="{{frontAsset('js/wow.min.js')}}"></script>
<script src="{{frontAsset('js/scripts.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>


<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>

@stack("js")
{!! Toastr::message() !!}
</body>

<!-- Mirrored from themesground.com/marazzo-demo/V3/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Feb 2020 20:44:29 GMT -->
</html>
