<?php

use Illuminate\Support\Str;
use App\Repositories\Classes\ProductImageRepository as ImageRepository;


function panelView($view = ""){

    $panelViewPath = "panel.".$view;

    return view($panelViewPath);
}

function panelAsset($asset = "") {

    $panelAssetPath = "/panel/assets/".$asset;

    return asset($panelAssetPath);
}

function getProfileImage($image = null){

    if($image == null)
    {
        return url("uploads/default_avatar.png");
    }

    return url("storage/uploads/users/profile-pictures/".$image);
}
function getCategoryImageWithDefault($image){
    if ($image == null) return "https://via.placeholder.com/100x100png?text=Kategori+Resmi";

    return url("storage/uploads/product-categories/".$image);
}
function getShortString(string $string, int $limit){
    return Str::limit($string,$limit,"...");
}

function getCoverImage($productID){

    $productImage = new ImageRepository();

    $imagesFromDB = $productImage->getAllByProductId($productID);

    if($imagesFromDB->count() <= 0)
    {
        return "https://via.placeholder.com/100x100png?text=Ürün+Kapak+Resmi";
    }

    foreach ($imagesFromDB as $image){

        return url("storage/uploads/products/".$image->image);

        break;
    }
}

function getBlogCover($image){

    if($image == null or $image == "")
        return "https://via.placeholder.com/100x100png?text=Blog+Kapak+Resmi";

    return url("storage/uploads/blogs/" . $image);
}

function getProductImage($image){
    return url("storage/uploads/products/".$image);
}

function getSliderImage($image){
    return url("storage/uploads/sliders/".$image);
}
