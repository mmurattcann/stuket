<?php

namespace App\models;

use App\Product;
use App\Traits\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    use ActiveScope;

    protected $guarded = ["id"];
    protected $fillable = ["title", "university_id"];

    public function university(){
        return $this->belongsTo(University::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
