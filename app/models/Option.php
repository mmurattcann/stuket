<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded = ["id"];
    protected $fillable = [
        "site_title",
        "logo",
        "favicon",
        "phone",
        "gsm",
        "contact_email",
        "info_email",
        "address",
        "google_map",
        "site_key",
        "secret_key"
    ];
}
