<?php

namespace App\models;

use App\Product;
use App\Traits\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    use ActiveScope;

    protected $guarded = ["id"];
    protected $fillable = ["title", "is_active", "code"];

    public function faculties(){
        return $this->hasMany(Faculty::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
