<?php

namespace App;

use App\Enums\ProductEnums;
use App\models\Faculty;
use App\models\University;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function owner(){
        return $this->belongsTo(User::class,"user_id");
    }
    public function images(){
        return $this->hasMany(ProductImage::class, "product_id");
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class,"category_id");
    }

    public function university(){
        return $this->belongsTo(University::class);
    }
    public function faculty(){
        return $this->belongsTo(Faculty::class);
    }

    public function scopeActive($query){
        return $query->where("is_active", ProductEnums::_ACTIVE_PRODUCT);
    }

}
