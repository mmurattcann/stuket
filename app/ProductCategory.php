<?php

namespace App;

use App\Enums\CategoryEnums;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $guarded = [];

    private $categoryEnum = null;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->categoryEnum = new CategoryEnums();
    }

    public function childs(){
        return $this->hasMany(ProductCategory::class,"parent_id");
    }

    public function parent(){
        return $this->belongsTo(ProductCategory::class,"parent_id")->where("parent_id", null)->with("parent");
    }

    public function products(){
        return $this->hasMany(Product::class, "category_id");
    }

    public function scopeParent($query){
        return $query->where("parent_id", null);
    }

    public function scopeChild($query){
        return $query->whereNotNull("parent_id");
    }

    public function scopeActive($query){
        return $query->where("is_active", $this->categoryEnum::_ACTIVE_CATEGORY);
    }

    public function getTypeAttribute($value){
        if($this->parent_id == null)
            return $this->categoryEnum::_PARENT_CATEGORY_STRING;

        return $this->categoryEnum::_CHILD_CATEGORY_STRING;
    }
}
