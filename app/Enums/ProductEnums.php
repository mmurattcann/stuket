<?php


namespace App\Enums;


class ProductEnums
{

    const _ACTIVE_PRODUCT   = 1;
    const _INACTIVE_PRODUCT = 0;

    const _ACTIVE_PRODUCT_STRING   = "Aktif";
    const _INACTIVE_PRODUCT_STRING = "Pasif";

    const _PRODUCT_IMAGE_COVER = 1;
}
