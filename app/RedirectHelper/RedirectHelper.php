<?php


namespace App\Helpers\RedirectHelper;
use Brian2694\Toastr\Facades\Toastr;

class RedirectHelper
{
    public static function RedirectWithSuccessFlashMessage(string  $operationType, string $route, $routeParams = [])
    {
        $title      = "Başarılı";
        $flashTitle = "success";
        $message    = null;

        switch($operationType){
            case "store":
                $message = "Kayıt Başarıyla Eklendi.";
                Toastr::success($message, $title);
                break;
            case "update":
                $message = "Kayıt Başarıyla Güncellendi.";
                Toastr::success($message, $title);
                break;
            case "destroy":
                $message = "Kayıt Başarıyla Silindi.";
                Toastr::success($message, $title);
                break;
            case "login":
                $message = "Hoşgeldiniz";
                Toastr::success($message, $title);
                break;
            case "register":
                $message = "Hesabınız Aktfileştirildi";
                Toastr::success($message, "Tebrikler!");
                break;
            case "contact":
                $message = "Mesajınız Gönderildi, Teşekkürler!";
                Toastr::success($message, "Teşekkürler!");
                break;
        }
        if($route != null) return redirect()->route($route, $routeParams)->with($flashTitle,$message);
        else return redirect()->back()->with($flashTitle,$message);
    }
    public static function RedirectWithErrorFlashMessage(string  $operationType,string $route = "")
    {
        $title      = "Hata!";
        $flashTitle = "error";
        $message    = null;

        switch($operationType){
            case "store":
                $message = "Kayıt Eklenirken Bir Sorun Oluştu.";
                Toastr::error($message, $title);
                break;
            case "update":
                $message = "Kayıt Güncellenirken Bir Sorun Oluştu.";
                Toastr::error($message, $title);
                break;
            case "destroy":
                $message = "Kayıt Silinirken Bir Sorun Oluştu.";
                Toastr::error($message, $title);

                break;
            case "password":
                $message = "Şifreler eşleşmiyor.";
                Toastr::error($message, $title);
            case "inactive":
                $message = "Aktivasyonunuz gerçekleşmemiş.";
                Toastr::error($message, $title);
            case "notAllowed":
                $message = "Bu işlem için yetkiniz yok.";
                Toastr::error($message, $title);
        }

        if($route != null) return redirect()->route($route)->with($flashTitle,$message);
        else return redirect()->back()->with($flashTitle,$message);
    }
    public static function RedirectWithWarningFlashMessage(string $route)
    {
        $title      = "Uyarı";
        $flashTitle = "warning";
        $message    = null;


        $message = "Bilgileri Tekrar Kontrol Edin.";

        Toastr::warning($message, $title);


        return redirect()->route($route)->with($flashTitle,$message);
    }
}
