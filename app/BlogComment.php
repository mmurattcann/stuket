<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $guarded = ["id"];

    public function post(){
        return $this->belongsTo(Blog::class, "post_id");
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function replies(){
        return $this->hasMany(BlogComment::class,"comment_id");
    }

    public function getModifiedDateAttribute(){

        $carbon = new Carbon();
        return  $carbon->locale("tr")->diffForHumans($this->created_at);
    }
}
