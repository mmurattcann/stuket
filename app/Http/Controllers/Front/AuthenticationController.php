<?php

namespace App\Http\Controllers\Front;

use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\UserCreateRequest;
use App\Notifications\VerifyTokenEmail;
use App\Repositories\Classes\BlogRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\SliderRepository;
use App\Repositories\Classes\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Murattcann\LaraImage\LaraImage;

class AuthenticationController extends Controller
{
    private $userRepository = null;
    private $sliderRepository = null;
    private $productRepository = null;
    private $blogRepository = null;

    private $resource       = "authentication";
    private $uploadPath     = "users/profile-pictures";

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->sliderRepository = new SliderRepository();
        $this->productRepository = new ProductRepository();
        $this->blogRepository = new BlogRepository();
    }

    public function index(){
        $data = [
            "title" => "Giriş Yap / Kaydol"
        ];
        return frontView("$this->resource.authentication")->with($data);
    }

    public function login(LoginRequest $request){
        $credentials = $request->only("email" , "password");

        $checkIsActive = $this->userRepository->baseQuery()->active()->where("email", $request->email)->first();
        $inActiveUser  = $this->userRepository->baseQuery()->where("email", $request->email)->first();

        if($checkIsActive)
        {
            if(Auth::attempt($credentials))
                return  RedirectHelper::RedirectWithSuccessFlashMessage("login", "front.index");
        }
        else{
            Notification::route("mail", $inActiveUser->email)->notify(new VerifyTokenEmail($inActiveUser));
            return $this->verifyCode($inActiveUser->email)->with("inActive", "Hesabınız Aktif Değil.");
        }

        return redirect()->back()->with("loginError", "E-Posta ya da Şifre hatalı");
    }

    public function register(UserCreateRequest $request){

        $requestData = $request->all();
        $image = $request->file("image");

        $user = $this->userRepository->baseQuery()->where("email",  $requestData["email"])->first();

        if($user){
           return redirect()->back()->with("error", "Bu email ile zaten kayıtlı bir hesap mevcut.");
        }
       $user = [
            "name"  => $requestData["name"],
            "email" => $requestData["email"].$requestData["email_extension"],
            "password" => Hash::make($requestData["password"]),
            "phone" => $requestData["phone"],
            "image" => LaraImage::upload("store", $this->uploadPath, $requestData["name"], $image, 150, 150),
            "about" => $requestData["about"],
            "verify_token" => Str::random(6),
            "is_active" => 0
        ];

        $createdUser = $this->userRepository->baseQuery()->create($user);

        Notification::route("mail", $createdUser->email)->notify(new VerifyTokenEmail($createdUser));


        return redirect()->route("front.verify-code", ["email" => $createdUser->email]);
    }

    public function verify(Request $request){

        $verify_token = $request->get("verify_token");
        $user_email   = $request->get("user_email");

        $checkUserExistsStatus  = $this->userRepository->baseQuery()
            ->where("email",$user_email)
            ->where("verify_token", $verify_token)
            ->where("is_active", 1)
            ->first();

        if($checkUserExistsStatus){
            return redirect()->back()->with("error", "Zaten Aktif Bir Hesaba Sahipsiniz.");
        }

        $checkUserToValidate = $this->userRepository->baseQuery()
            ->where("email",$user_email)
            ->where("verify_token", $verify_token)
            ->where("is_active", 0)
            ->first();

        if($checkUserToValidate){
            $checkUserToValidate->update(["is_active" => 1]);
        }

        return RedirectHelper::RedirectWithSuccessFlashMessage("register", "front.authentication")
            ->with("success", "Hesabınız Aktifleştirildi! Lütfen bilgilerinizle giriş yapın.");

    }

    public function verifyCode($userEmail){
        $viewData = [
            "title" => "Hesap Aktivasyonu",
            "message" => "Lütfen e-posta adresinize gönderilen doğrulama kodunu girin.",
            "user_email" => $userEmail,
        ];

        return frontView("$this->resource.verify")->with($viewData);
    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return redirect()->route("front.index");
    }

    public function reVerify($userId){

        $user = $this->userRepository->getById($userId);
        Notification::route("mail", $user->email)->notify(new VerifyTokenEmail($user));

        return $this->verifyCode($user->email);
    }
}
