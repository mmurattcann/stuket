<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\FacultyRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\UniversityRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categoryRepository = null;
    private $productRepository  = null;
    private $universityRepository  = null;
    private $facultyRepository     = null;

    public function __construct()
    {
        $this->categoryRepository = new ProductCategoryRepository();
        $this->productRepository  = new ProductRepository();
        $this->universityRepository = new UniversityRepository();
        $this->facultyRepository = new FacultyRepository();
    }

    public function index($slug){
        $category = $this->categoryRepository->getBySlug($slug);
        $products = $this->productRepository->listingMap($category->products);
        $categories = $this->categoryRepository->getAllActive();

        $data = [
            "title" => $category->title. " Kategorisine Ait Ürünler",
            "products" => $products,
            "categories" => $categories,
            "hasCategory" => true,
            "categoryTitle" => $category->title,
            "universities" =>  $this->universityRepository->getAll("title", "asc"),
            "faculties" => $this->facultyRepository->getAll("title", "asc")
        ];

        return frontView("products.product-list")->with($data);
    }
}
