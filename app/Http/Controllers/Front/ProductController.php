<?php

namespace App\Http\Controllers\Front;

use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\ExtendedClasses\FrontProductRepositoryExtended;
use App\Repositories\Classes\ExtendedClasses\ProductRepositoryExtended;
use App\Repositories\Classes\FacultyRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductImageRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\UniversityRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Murattcann\LaraImage\LaraImage;

class ProductController extends Controller
{
    private $repository = null;
    private $frontProductRepository = null;
    private $productImageRepository = null;
    private $extendedRepository = null;
    private $categoryRepository = null;
    private $universityRepository = null;
    private $facultyRepository    = null;
    private $resource = "products";

    public function __construct()
    {
        $this->repository = new ProductRepository();
        $this->frontProductRepository = new FrontProductRepositoryExtended();
        $this->productImageRepository = new ProductImageRepository();
        $this->extendedRepository = new ProductRepositoryExtended();
        $this->categoryRepository = new ProductCategoryRepository();
        $this->universityRepository = new UniversityRepository();
        $this->facultyRepository    = new FacultyRepository();
    }

    public function index(){
        $products =  $this->repository->listingMap($this->repository->getAllActive("id", "desc"));
        $categories = $this->categoryRepository->getAll("title", "asc")->toArray();

        $data = [
            "title" => "Ürünler",
            "products" => $products,
            "categories" => $categories,
            "hasCategory" => false,
            "universities" =>  $this->universityRepository->getAll("title", "asc"),
            "faculties" => $this->facultyRepository->getAll("title", "asc")
        ];

        return frontView("$this->resource.product-list")->with($data);
    }

    public function detail($slug){

        $product = $this->repository->detailMap($this->repository->getBySlug($slug));

        $similarProducts = $this->repository
            ->listingMap($this->repository->baseQuery()
                ->active()
                ->where("id", "!=", $product["id"])
                ->take(6)
                ->get());

        $data = [
            "title" => $product["title"],
            "product" => $product,
            "similarProducts" => $similarProducts
        ];

        return frontView("$this->resource.product-detail")->with($data);
    }

    public function search(Request $request){
        $request->flash();

        $title = "Ürünler";
        $category = null;
        $categoryTitle = "";

        $hasCategory = false;
        $productsByCategory = [];
        $productsByUniversity = [];
        $productsByFaculty    = [];
        $productsByLowestPrice = [];
        $productsByHighestPrice = [];
        $productsByTitle = [];

        if($request->get("category_id")){
            $categoryID = $request->get("category_id");
            $category = $this->categoryRepository->getById($categoryID);
            $categoryTitle = $category->title;
            $title =  $categoryTitle." Kategorisine Ait Ürünler";
            $productsByCategory     = $this->extendedRepository->filterByCategory($categoryID)->toArray();

            if($productsByCategory) $hasCategory = true;
        }

        if($request->get("university_id")){
            $universityID = $request->get("university_id");
            $university = $this->universityRepository->getById($universityID);
            $title      = $university->title. " Konumundaki Ürünler";
            $productsByUniversity = $this->extendedRepository->filterByUniversity($universityID)->toArray();
        }

        if($request->get("faculty_id")){
            $facultyID = $request->get("faculty_id");
            $faculty = $this->facultyRepository->getById($facultyID);
            $title      = $faculty->title. " Konumundaki Ürünler";
            $productsByFaculty = $this->extendedRepository->filterByFaculty($facultyID)->toArray();
        }

        if($request->get("lowest_price"))
            $productsByLowestPrice  = $this->extendedRepository->filterByLowestPrice($request->get("lowest_price"))->toArray();


        if($request->get("highest_price"))
            $productsByHighestPrice = $this->extendedRepository->filterByHighestPrice($request->get("highest_price"))->toArray();

        if($request->get("query")){
            $productsByTitle = $this->extendedRepository->filterByTitle($request->get("query"))->toArray();
            $title = $request->get("query"). " Aramasına İlişkin Sonuçlar";
        }

        $mergedProducts = array_merge($productsByCategory, $productsByLowestPrice, $productsByHighestPrice, $productsByTitle, $productsByUniversity, $productsByFaculty);


        $categories = $this->categoryRepository->getAll("title", "asc");


        $data = [
            "title" => $title,
            "products" => $mergedProducts,
            "categories" => $categories,
            "isSearchPage" => true,
            "hasCategory"  => $hasCategory,
            "categoryTitle" => $categoryTitle,
            "universities" =>  $this->universityRepository->getAll("title", "asc"),
            "faculties" => $this->facultyRepository->getAll("title", "asc")

        ];

        return frontView("$this->resource.product-list")->with($data);
    }

    public function create(){

        $categories = $this->categoryRepository->getAll("title", "asc");
        $universities = $this->universityRepository->getAll("title", "asc");
        $faculties = $this->facultyRepository->getAll("title", "asc");

        $data = [
            "title" => "Birinci Adım: Ürün Bilgileri",
            "categories" => $categories,
            "user" => Auth::user(),
            "universities" => $universities,
            "faculties" => $faculties
        ];

        return frontView("users.add-product")->with($data);
    }

    public function edit($id){

        $product = $this->repository->detailMap($this->repository->getById($id));
        $categories = $this->categoryRepository->getAllActive();
        $universities = $this->universityRepository->getAll("title", "asc");
        $faculties = $this->facultyRepository->getAll("title", "asc");

        $data = [
            "title" => $product["title"]. " Başlıklı Ürün Düzenleniyor",
            "product" => $product,
            "categories" => $categories,
            "user" => Auth::user(),
            "universities" => $universities,
            "faculties" => $faculties

        ];

        return frontView("users.edit-product")->with($data);
    }

    public function stepTwo(Request $request){

        Session::put("productCredentials",$request->all());
        //Session::get("productCredentials");

        $data = [
            "title" => "İkinci Adım: Ürün Resimleri",
            "user" => Auth::user(),
        ];

        return frontView("users.add-product-step-two")->with($data);
    }

    public function editStepTwo(Request $request, $id){

        Session::put("product", $request->all());

        $product = $this->repository->getById($id);

        $product->update($request->all());

        $data = [
            "title" => $product->title. " Ürününe Ait Resimler Düzenleniyor",
            "product" => $product,
            "images" => $product->images,
            "user"   => Auth::user(),

        ];

        return frontView("users.edit-product-step-two")->with($data);
    }

    public function completeSteps(Request $request){


        if($request->file("file"))
        {

            $image = $request->file("file");

            $productCredentials = Session::get("productCredentials");

            $this->frontProductRepository->storeWithArray($productCredentials);

            $this->frontProductRepository->storeFrontProductImage($image);

            //Session::flush();
            return  RedirectHelper::RedirectWithSuccessFlashMessage("store","front.profile",["userId" => Auth::user()->id]);
        }

        return redirect()->back()->with("error", "Resim yüklemeden ilan oluşturamazsınız.");

    }

    public function update(Request $request, $id){

        $this->frontProductRepository->storeFrontProductImage($request->file("file"), $id);
        return RedirectHelper::RedirectWithSuccessFlashMessage("update", "front.profile", ["userId" => Auth::user()->id]);
    }

    public function destroy($id){
        $this->repository->destroy($id);
        return response()->json(["message" => "Ürün Başarıyla Silindi"], 200);
    }

    public function deleteImage($id){

        $image =  $this->productImageRepository->baseQuery()->where("id", $id)->first();

        LaraImage::deleteUploadedFile($this->repository->uploadPath, $image->image);

        return response()->json(["message" => "Kayıt Başarıyla Silindi"]);
    }
}
