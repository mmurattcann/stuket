<?php

namespace App\Http\Controllers\Front;

use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Notifications\ClientToSellerNotification;
use App\Notifications\ContactFormNotification;
use App\Repositories\Classes\OptionRepository;
use App\Repositories\Classes\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContactController extends Controller
{
    private $userRepository = null;
    private $optionRepository = null;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->optionRepository = new OptionRepository();
    }

    public function sendMailToSeller(Request $request){
        $user = $this->userRepository->getById($request->get("user_id"));

        Notification::route("mail", $user->email)->notify(new ClientToSellerNotification($request));
        return redirect()->back()->with("success","Mesajınız Gönderildi.");
    }

    public function contactPage(){
        $data = [
            "title" => "İletişim"
        ];
        return frontView("contact")->with($data);
    }

    public function sendContactMessage(Request $request){

        $contactEmail = $this->optionRepository->getFirst()->contact_email;
        Notification::route("mail", $contactEmail)->notify(new ContactFormNotification($request));

        return RedirectHelper::RedirectWithSuccessFlashMessage("contact", "front.contact-page");
    }
}
