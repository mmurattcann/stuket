<?php

namespace App\Http\Controllers\Front;

use App\BlogComment;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogCategoryRepository;
use App\Repositories\Classes\BlogRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private  $blogRespository = null;
    private  $categoryRepository = null;
    private  $resource = "blog";

    public function __construct()
    {
        $this->blogRespository = new BlogRepository();
        $this->categoryRepository = new BlogCategoryRepository();
    }

    public function index(){

        $data = [
            "title" => "Blog Yazılarımız",
            "blogs" => $this->blogRespository->getAll("id", "desc")
        ];

        return frontView("$this->resource.blog-list")->with($data);

    }

    public function detail($slug){

        $blog = $this->blogRespository->getBySlug($slug);
        $comments = $blog->comments;
        $similarPosts = $this->blogRespository->getAllWithWhere("id","!=", $blog->id);

        $data = [
            "title" => $blog->title,
            "blog"  => $blog,
            "similarPosts" => $similarPosts,
            "comments"     => $comments
        ];

        return frontView("$this->resource.blog-detail")->with($data);
    }

    public function getBlogsByCategory($slug)
    {
        $category = $this->categoryRepository->getBySlug($slug);
        dd($category);
    }
}
