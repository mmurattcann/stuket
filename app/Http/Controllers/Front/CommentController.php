<?php

namespace App\Http\Controllers\Front;

use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\CommentRequest;
use App\Repositories\Classes\BlogCommentRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    private $repository = null;

    public function __construct()
    {
        $this->repository = new BlogCommentRepository();
    }

    public function store(CommentRequest $request){

        $this->repository->store($request);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store", "");
    }
}
