<?php

namespace App\Http\Controllers\Front;

use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userRepository = null;
    private $productRepository = null;
    private $resource = "users";
    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->productRepository = new ProductRepository();
    }

    public function profile($id){
        $products = null;

        $user = $this->userRepository->getById($id);

        if($user->products) $products = $this->productRepository->listingMap($user->products);

        $data = [
            "title" => $user->name. " Kullanıcısının İlanları",
            "user"  => $user,
            "products" =>  $products
        ];
        return frontView("$this->resource.user-profile")->with($data);
    }

    public function update(Request $request, $id){

        $user =$this->userRepository->update($id, $request);

        return RedirectHelper::RedirectWithSuccessFlashMessage("update", "front.profile",["userId" => $user->id]);
    }
}
