<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogCategoryRepository;
use App\Repositories\Classes\BlogRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\SliderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    private $categoryRepository = null;
    private $blogRepository = null;
    private $blogCategoryRepository = null;
    private $productRepository = null;
    private $sliderRepository = null;

    public function __construct()
    {
        $this->categoryRepository = new ProductCategoryRepository();
        $this->blogRepository     = new BlogRepository();
        $this->blogCategoryRepository = new BlogCategoryRepository();
        $this->productRepository  = new ProductRepository();
        $this->sliderRepository   = new SliderRepository();
    }

    public function index(){

        $indexProducts = $this->productRepository->listingMap($this->productRepository->getIndexProducts());
        $indexBlogs    = $this->blogRepository->listingMap($this->blogRepository->getIndexBlogs());
        $indexSliders  = $this->sliderRepository->listingMap($this->sliderRepository->getAll("rank", "asc"));

        $data = [
            "title" => "Anasayfa",
            "indexProducts" => $indexProducts,
            "indexBlogs" => $indexBlogs,
            "indexSliders" => $indexSliders
        ];

        return frontView("index")->with($data);
    }

}
