<?php

namespace App\Http\Controllers\Panel;

use App\Enums\BlogEnums;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\UniversityRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class UniversityController extends Controller
{
    use GeneralCrud, StatusUpdater;

    private $repository = null;
    private $resource = "universities";

    public function __construct()
    {
        $this->repository = new UniversityRepository();
    }

    public function index(){
        $universities = $this->repository->getAll();
        $data = [
            "title" => "Üniversite Yönetimi",
            "universities" => $universities,
            "isActive" => BlogEnums::_ACTIVE
         ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create(){
        $data = [
            "title" => "Yeni Üniversite Kaydı"
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function edit($id){
        $university = $this->repository->getById($id);

        $data = [
            "title" => $university->title. " Üniversitesi Düzenleniyor",
            "university" => $university
        ];

        return panelView("$this->resource.edit")->with($data);
    }
}
