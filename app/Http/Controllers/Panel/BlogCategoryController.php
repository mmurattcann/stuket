<?php

namespace App\Http\Controllers\Panel;

use App\Enums\BlogEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogCategoryRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    use GeneralCrud, StatusUpdater;
    private $repository = null;
    private $pageEnum   = null;
    private $resource   = null;

    public function __construct()
    {
        $this->repository = new BlogCategoryRepository();
        $this->pageEnum   = new BlogEnums();
        $this->resource   = "blog-categories";
    }
    public function index()
    {
        $data = [
            "title"    => "Blog Kategorisi Yönetimi",
            "categories"    => $this->repository->getAll(),
            "isActive" => $this->pageEnum::_ACTIVE,
        ] ;

        return panelView("$this->resource.index")->with($data);
    }


    public function create()
    {
        $data = [
            "title" => "Yeni Blog Kategorisi Oluştur",
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function edit($id)
    {
        $category = $this->repository->getById($id);

        $data = [
            "title" => $category->title .  " Kategorisi Düzenleniyor",
            "category"  => $category
        ];

        return panelView("$this->resource.edit")->with($data);
    }
}
