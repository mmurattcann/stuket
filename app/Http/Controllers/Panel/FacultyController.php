<?php

namespace App\Http\Controllers\Panel;

use App\Enums\BlogEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\FacultyRepository;
use App\Repositories\Classes\UniversityRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    use StatusUpdater;

    private $repository = null;
    private $universityRepository = null;
    private $resource = "faculties";

    public function __construct()
    {
        $this->repository = new FacultyRepository();
        $this->universityRepository = new UniversityRepository();
    }

    public function index($id){

        $university = $this->universityRepository->getById($id);
        $data = [
            "title" => $university->title. " Fakülte Listesi",
            "university" => $university,
            "faculties" => $university->faculties,
            "isActive" => BlogEnums::_ACTIVE
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create($id){
        $university = $this->universityRepository->getById($id);

        $data = [
            "title" => "Yeni Fakülte Kaydı",
            "university" => $university
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function store(Request $request){

        $university_id = $request->get("university_id");
        $this->repository->store($request);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store", "$this->resource.index", ["id" => $university_id]);
    }
    public function edit($parentID, $id){

        $university = $this->universityRepository->getById($id);
        $faculty = $this->repository->getById($id);

        $data = [
            "title" => $faculty->title. " Fakültesi Düzenleniyor",
            "university" => $university,
            "faculty" => $faculty
        ];

        return panelView("$this->resource.edit")->with($data);
    }

    public function update(Request $request, $parentID, $id){
        $university_id = $request->get("university_id");
        $this->repository->update($request, $id);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store", "$this->resource.index", ["id" => $university_id]);
    }

    public function destroy($id){
        $this->repository->destroy($id);
        return response()->json(["message" => "Kayıt Başarıyla Silindi"], 200);
    }
}
