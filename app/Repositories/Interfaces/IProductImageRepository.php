<?php


namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

interface IProductImageRepository
{
    public function baseQuery();

    public function getById(Int $id);

    public function getAll($orderBy = ["id", "asc"]);

    public function getAllByProductId(int $id, $orderBy = ["id", "asc"]);

    public function store($id, Request $request);

    public function update($id, Request $request);

    public function delete($id);

    public function setCover($id);

}
