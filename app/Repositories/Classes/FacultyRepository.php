<?php
namespace App\Repositories\Classes;

use App\Enums\BlogEnums;
use App\models\Faculty;
use App\models\University;
use App\Repositories\Interfaces\IBaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FacultyRepository implements IBaseRepository
{
    protected $model;
    protected $enum;

    public function __construct()
    {
        $this->model = new Faculty();
        $this->enum  = new BlogEnums();
    }

    public function baseQuery()
    {
        return $this->model::query()->with("university");
    }

    public function getById(int $id)
    {
        return $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        //
    }

    public function getAll(string $order = "id", string $by = "asc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }

    public function getAllByParentID($parentID)
    {
        return $this->baseQuery()->where("university_id")->orderBy("title", "asc")->get();
    }

    public function store(Request $request)
    {

        $data = $request->all();

        return  $this->baseQuery()->create($data);
    }

    /*
     * You can also modify this request
     *  For Example;
     *
     *  $data["name"] = Str::upper("john doe");
     */
    public function update(Request $request, int $id)
    {

        $model = $this->getById($id);

        $data = $request->all();

        return $model->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $page = $this->getById($id);

        $active = $this->enum::_ACTIVE;

        $inActive = $this->enum::_INACTIVE;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $page->is_active = $is_active;

        return $page->save();
    }
    public function destroy(int $id)
    {

        $model = $this->getById($id);

        return $model->delete();
    }
}
