<?php


namespace App\Repositories\Classes;


use App\models\Option;
use App\Repositories\Interfaces\IOptionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Murattcann\LaraImage\LaraImage;

class OptionRepository implements IOptionRepository
{
    protected $model = null;
    protected $logoUploadPath = "uploads/options/logo/";
    protected $faviconUploadPath = "uploads/options/favicon/";
    protected $logoWidth      = null;
    protected $logoHeight     = null;

    public function __construct()
    {
        $this->model = new Option();
        $this->logoWidth  = config("imageUpload.logo.width");
        $this->logoHeight = config("imageUpload.logo.height");
    }

    public function baseQuery()
    {
        return $this->model::query();
    }

    public function getById(int $id)
    {
        return $this->baseQuery()->where('id', $id)->first();
    }

    public function getFirst(){
        return $this->baseQuery()->first();
    }

    public function  update(Request $request, int $id)
    {
        $category = $this->getById($id);

        $data = $request->only($this->model->getFillable());

        if($request->has("logo"))
            $data["logo"] = LaraImage::upload("update", $this->logoUploadPath, "logo-", $data["logo"], $this->logoWidth, $this->logoHeight);

        if($request->has("favicon"))
            $data["favicon"] = LaraImage::upload("update", $this->faviconUploadPath, "favicon-", $data["favicon"], $this->logoWidth, $this->logoHeight);

        $category->update($data);

        Cache::forget('options');
    }

    public function detailMap($item){

        return [
            "id" => $item->id,
            "site_title" => $item->site_title,
            "address" => $item->address,
            "gsm" => $item->gsm,
            "phone" => $item->phone,
            "contact_email" => $item->contact_email,
            "info_email" => $item->info_email,
            "site_key" => $item->site_key,
            "secret_key" => $item->secret_key,
            "logo" => getLogo($item->logo),
            "favicon"     => getFavicon($item->favicon)

       ];

    }
}
