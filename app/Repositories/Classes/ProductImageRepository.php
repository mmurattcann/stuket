<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Product;

use App\ProductImage;
use App\Repositories\Interfaces\IProductImageRepository;
use Illuminate\Http\Request;
use Murattcann\LaraImage\LaraImage;

class ProductImageRepository implements IProductImageRepository
{
    private $productImage  = null;
    private $productEnum   = null;
    private $relations     = null;
    private $uploadPath    = "uploads/products";

    public function __construct()
    {
        $this->productImage = new ProductImage();
        $this->productEnum = new ProductEnums();
        $this->relations = ["product"];
    }

    public function baseQuery()
    {
        return $this->productImage::query();
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getAll($orderBy = ["id", "desc"])
    {
        return $this->baseQuery()->with($this->relations)->orderBy($orderBy[0],$orderBy[1])->get();
    }
    public function getAllByProductId(int $id, $orderBy = ["id", "desc"])
    {
        return $this->baseQuery()
            ->where("product_id","$id")
            ->orderBy($orderBy[0],$orderBy[1])
            ->get();
    }
    public function store($id, Request $request)
    {

        $data = $request->all();
        return $this->productImage->create($data);
    }

    public function update($id, Request $request)
    {


        $product = $this->getById($id);

        $isActive = $product->is_active == $this->productEnum::_ACTIVE_PRODUCT ? $this->productEnum::_ACTIVE_PRODUCT : $this->productEnum::_INACTIVE_PRODUCT;

        $data = $request->all();

        if($request->hasFile("image")){

            $image = $request->file("image");
            $data["is_active"] = $isActive;
            $data["image"] = LaraImage::upload("update",$this->uploadPath,$data["title"], $image, 500, 500);

        }

        $product->update($data);
    }
    public function setCover($id)
    {

        $request = new Request();

        $product = $this->getById($id);

        $active = $this->productEnum::_ACTIVE_PRODUCT;

        $inActive = $this->productEnum::_INACTIVE_PRODUCT;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $product->is_active = $is_active;

        return $product->save();
    }

    public function delete($id)
    {
        $image = $this->getById($id);

        LaraImage::deleteUploadedFile($this->uploadPath, $image->image);

        $image->delete();
    }
}
