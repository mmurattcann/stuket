<?php


namespace App\Repositories\Classes;


use App\Enums\CategoryEnums;
use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Product;
use App\ProductCategory;
use App\ProductImage;
use App\Repositories\Interfaces\IBaseRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Murattcann\LaraImage\LaraImage;

class ProductRepository implements IBaseRepository
{
    protected $product       = null;
    protected $productEnum   = null;
    protected $relations     = null;
    public    $uploadPath    = "uploads/products";
    protected $imageModel    = null;
    protected $imageWidth  = null;
    protected $imageHeight = null;

    public function __construct()
    {
        $this->product     = new Product();
        $this->productEnum = new ProductEnums();
        $this->relations   = ["owner", "category", "images"];
        $this->imageModel  = new ProductImage();
        $this->imageWidth   = config("imageUpload.product.width");
        $this->imageHeight  = config("imageUpload.product.height");
    }

    public function baseQuery()
    {
        return $this->product::query()->with($this->relations);
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->where("slug", $slug)->first();
    }

    public function getAll($column = "id", $order= "desc")
    {
        return $this->baseQuery()->orderBy($column, $order)->get();
    }

    public function getAllWithPaginate(){
        return $this->baseQuery()->orderBy("id", "desc")->paginate(4);
    }

    public function getAllActive($column = "id", $order= "desc")
    {
        return $this->baseQuery()->orderBy($column, $order)->active()->get();
    }

    public function getIndexProducts(){
        return $this->baseQuery()->active()->take(12)->get();
    }

    public function getMinimumPrice(){
        return $this->baseQuery()->min("price");
    }

    public function getMaximumPrice(){
        return $this->baseQuery()->max("price");
    }
    public function store(Request $request)
    {

        $data = $request->all();
        if($data["slug"] == null){
             $data["slug"] = Str::slug($data["title"], "-");
        }

        return $product = $this->product->create($data);
    }

    public function storeProductImage(Request $request)
    {

        $current_id = null;
        $image = $request->file("file");


        $statement = DB::select("show table status like 'products'");

        if($request->has("product_id")) $current_id = $request->product_id;
        else $current_id = $statement[0]->Auto_increment;


        $title = "Product($current_id)";

        $data = [
            "title" => $title,
            "image" => LaraImage::upload("store", $this->uploadPath,$title, $image,$this->imageWidth, $this->imageHeight),
            "is_cover" => 0,
            "product_id" => $current_id
        ];

        $images = $this->imageModel->create($data);
    }

    public function update(Request $request,$id )
    {


        $product = $this->getById($id);

        $isActive = $product->is_active == $this->productEnum::_ACTIVE_PRODUCT ? $this->productEnum::_ACTIVE_PRODUCT : $this->productEnum::_INACTIVE_PRODUCT;

        $data = $request->all();
        if($data["slug"] == null){
            $data["slug"] = Str::slug($data["title"], "-");
        }

        $product->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $product = $this->getById($id);

        $active = $this->productEnum::_ACTIVE_PRODUCT;

        $inActive = $this->productEnum::_INACTIVE_PRODUCT;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $product->is_active = $is_active;

        return $product->save();
    }

    public function destroy(int $id)
    {
        $product = $this->getById($id);
        foreach ($product->images as $image)
        {
            LaraImage::deleteUploadedFile($this->uploadPath, $image);
        }

        $product->delete();
    }

    public function listingMap($collect){
        return $collect->map(function ($item){
            return [
                "id" => $item->id,
                "title" => $item->title,
                "slug"  => $item->slug,
                "description" => $item->description,
                "short_description" => Str::limit($item->description, 60, "..."),
                "address" => $item->address,
                "categoryTitle" => $item->category->title,
                "price" => $item->price,
                "image" => getCoverImage($item->id),
                "ownerID" => $item->owner->id,
                "route" => route("front.product-detail", ["slug" => $item->slug])
            ];
        });
    }

    public function detailMap($item){
        return [
            "id" => $item->id,
            "title" => $item->title,
            "slug" => $item->slug,
            "description" => strip_tags($item->description),
            "price" => $item->price,
            "address" => $item->address,
            "images" => $item->images,
            "categoryID" => $item->category->id,
            "ownerPhone" => $item->owner->phone,
            "ownerName" => $item->owner->name,
            "ownerEmail" => $item->owner->email,
            "ownerID" => $item->owner->id,
            "ownerImage" => getProfileImage($item->owner->image),
            "university_id" => $item->university->id,
            "faculty_id" => $item->faculty->id
        ];
    }
}
