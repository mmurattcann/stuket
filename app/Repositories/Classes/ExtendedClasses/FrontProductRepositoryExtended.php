<?php


namespace App\Repositories\Classes\ExtendedClasses;


use App\Repositories\Classes\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Murattcann\LaraImage\LaraImage;

class FrontProductRepositoryExtended extends ProductRepository
{
    // this methods for front product operations

    public function storeWithArray ($data){

        $data["slug"] = Str::slug($data["title"], "-");
        $data["user_id"] = Auth::user()->id;
        return  $this->baseQuery()->create($data);
    }
    public function updateWithArray($data){

        $data["slug"] = Str::slug($data["title"], "-");
        $data["user_id"] = Auth::user()->id;

        $this->baseQuery()->update($data);
    }

    public function storeFrontProductImage($image, $id = null)
    {

        $current_id = null;

        $statement = DB::select("show table status like 'products'");

        if($id != null)
            $current_id = $id;
        else
            $current_id = $statement[0]->Auto_increment;


        $title = "Product($current_id)";

        $data = [
            "title" => $title,
            "image" => LaraImage::upload("store", $this->uploadPath,$title, $image,$this->imageWidth, $this->imageHeight),
            "is_cover" => 0,
            "product_id" => $current_id
        ];

       return  $images = $this->imageModel->create($data);
    }

}
