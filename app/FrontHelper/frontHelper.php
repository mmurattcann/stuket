<?php

function frontView($view = ""){

    $panelViewPath = "frontend.views.".$view;

    return view($panelViewPath);
}

function frontAsset($asset = "") {

    $panelAssetPath = "/frontend/assets/".$asset;

    return asset($panelAssetPath);
}

function isActiveMenu($route){
    if(request()->is($route))
        return true;
    else
        return false;
}

function getLogo($image = null){

    if($image == null) return url("uploads/logo/logo.png");

    return url("storage/uploads/options/logo/".$image);
}

function getFavicon($image){
    if($image != null) return url("storage/uploads/options/favicon/".$image);

    return "Tanımlı Değil";
}
