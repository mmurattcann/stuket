<?php


namespace App\Traits;


use App\Helpers\RedirectHelper\RedirectHelper;
use Illuminate\Http\Request;

trait GeneralCrud
{
    public function store(Request $request){
        $this->repository->store($request);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store", "$this->resource.index");
    }
    public function update(Request $request, $id){
        $this->repository->update($request, $id);
        return RedirectHelper::RedirectWithSuccessFlashMessage("update", "$this->resource.index");
    }

    public function destroy($id){
        $this->repository->destroy($id);
        return response()->json(["message" => "Kayıt Başarıyla Silindi"], 200);
    }
}
