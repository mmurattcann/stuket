<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PANEL ROUTES

Route::group(["namespace" => "Panel", "prefix" => "panel", "middleware" => ["auth", "admin"]],function (){

    Route::get('dashboard', 'DashboardController@index')->name("dashboard");

    Route::get("options", "OptionController@edit")->name("options.edit");
    Route::put("options/update/{id}", "OptionController@update")->name("options.update");

    Route::resource("users", "UserController");
    Route::post("updateUserStatus", "UserController@updateUserStatus")->name("updateUserStatus");

    Route::resource("product-categories", "ProductCategoryController");
    Route::post("product-categories/update-category-status", "ProductCategoryController@updateStatus")->name("updateProductCategoryStatus");

    Route::resource("products", "ProductController");

    Route::get("product-images", "ProductImageController@index")->name("product-images.index");
    Route::post("product-images/store", "ProductController@storeProductImage")->name("product-images.store");
    Route::post("updateProductStatus", "ProductController@updateStatus")->name("updateProductStatus");
    Route::delete("product-images/{id}", "ProductImageController@destroy")->name("product-images.destroy");

    Route::resource("pages","PageController");
    Route::post("updatePageStatus", "PageController@updateStatus")->name("updatePageStatus");


    Route::resource("blog-categories","BlogCategoryController");
    Route::post("updateBlogCategoryStatus", "BlogCategoryController@updateStatus")->name("updateBlogCategoryStatus");


    Route::resource("blogs","BlogController");
    Route::post("updateBlogStatus", "BlogController@updateStatus")->name("updateBlogStatus");

    Route::resource("sliders", "SliderController");
    Route::post("updateSliderStatus", "SliderController@updateStatus")->name("updateSliderStatus");

    Route::resource("universities", "UniversityController");
    Route::post("updateUniversityStatus", "UniversityController@updateStatus")->name("updateUniversityStatus");

    Route::get("faculties/{id}", "FacultyController@index")->name("faculties.index");
    Route::get("faculties/create/{id}", "FacultyController@create")->name("faculties.create");
    Route::get("faculties/edit/{parentID}/{id}", "FacultyController@edit")->name("faculties.edit");
    Route::put("faculties/update/{parentID}/{id}", "FacultyController@update")->name("faculties.update");
    Route::post("faculties/store", "FacultyController@store")->name("faculties.store");
    Route::delete("faculties/delete/{id}", "FacultyController@destroy")->name("faculties.destroy");
    Route::post("updateFacultyStatus", "FacultyController@updateStatus")->name("updateFacultyStatus");
});

Route::get("logout", function (){
    Auth::logout();

    return redirect()->route("login");
});

Auth::routes(["verify" => true, "register" => false]);

Route::get('/home', 'HomeController@index')->name('home');

// FRONT ROUTES


Route::group(["namespace" => "Front"],function (){

    Route::get("/", "HomeController@index")->name("front.index");

    Route::get("kategori/{slug}", "CategoryController@index")->name("front.category");

    Route::get("blog", "BlogController@index")->name("front.get-all-blog");
    Route::get("blog/{slug}","BlogController@detail")->name("front.blog-detail");
    Route::get("blog/kategori/{slug}", "BlogController@getBlogsByCategory")->name("front.get-blogs-by-category");

    Route::post("blog/add-comment", "CommentController@store")->name("front.add-blog-comment");

    Route::get("urunler", "ProductController@index")->name("front.product-list");
    Route::get("urun/{slug}", "ProductController@detail")->name("front.product-detail");
    Route::post("urunler/ara", "ProductController@search")->name("front.search-product");

    Route::get("giris-yap", "AuthenticationController@index")->name("front.authentication");
    Route::post("giris-yap","AuthenticationController@login")->name("front.login");
    Route::post("kullanici-islemleri/kaydol","AuthenticationController@register")->name("front.register");

    Route::get("kullanici-islemleri/dogulama-kodu/{email}", "AuthenticationController@verifyCode")->name("front.verify-code");
    Route::post("kullanici-islemleri/dogrula", "AuthenticationController@verify")->name("front.verify");
    Route::post("kullanici-islemleri/tekrar-dogrula/{userId}", "AuthenticationController@reVerify")->name("front.re-verify");
    Route::get("kullanici-islemleri/cikis-yap", "AuthenticationController@logout")->name("front.logout");

    Route::get("profil/{userId}", "UserController@profile")->name("front.profile");
    Route::put("profil/güncelle/{id}", "UserController@update")->name("front.update-user");

    Route::get("urun-ekle/birinci-adim", "ProductController@create")->name("front.create-product");
    Route::post("urun-ekle/ikinci-adim", "ProductController@stepTwo")->name("front.product-step-two");
    Route::post("urun-ekle/kaydet", "ProductController@completeSteps")->name("front.product-complete-steps");

    Route::delete("urun-sil/{id}", "ProductController@destroy")->name("front.delete-product");

    Route::get("urun-duzenle/birinci-adim/{id}", "ProductController@edit")->name("front.edit-product");
    Route::put("urun-duzenle/ikinci-adim/{id}", "ProductController@editStepTwo")->name("front.edit-product-step-two");
    Route::put("urun-duzenle/tamamla/{id}", "ProductController@update")->name("front.update-product");
    Route::delete("urun-duzenle/resim-sil/{id}", "ProductController@deleteImage")->name("front.delete-product-image");

    Route::get("iletisim", "ContactController@contactPage")->name("front.contact-page");
    Route::post("iletisim/gonder", "ContactController@sendContactMessage")->name("front.contact");

    Route::post("sendMailToSeller", "ContactController@sendMailToSeller")->name("front.send-mail-to-seller");


    Route::get("testEmail", function (){
        return frontView("authentication.verify")->with(["title" => "verify", "message" => "uyarı mesajı", "user_email" => "asd"]);
    });
});
