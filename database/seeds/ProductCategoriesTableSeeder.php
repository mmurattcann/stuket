<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategoriesTableSeeder extends Seeder
{

    public function run()
    {
        $data = [
            [
                "title" => "Elektronik",
                "slug" => "elektronik",
                "description" => "Elektronik kategorisine ait açıklama metni.",
                "rank" => 1,
                "image" => null,
                "is_active" => 1,
                "parent_id" => null,
                "created_at" => "2020-01-06 01:01:32"
            ],
            [
                "title" => "Mobilya",
                "slug" => "mobilya",
                "description" => "Mobilya kategorisine ait açıklama metni.",
                "rank" => 1,
                "image" => null,
                "is_active" => 1,
                "parent_id" => null,
                "created_at" => "2020-01-06 01:01:32"
            ],
            [
                "title" => "Telefon",
                "slug" => "Telefon",
                "description" => "telefon kategorisine ait açıklama metni.",
                "rank" => 1,
                "image" => null,
                "is_active" => 1,
                "parent_id" => 1,
                "created_at" => "2020-01-06 01:01:32"
            ],
            [
                "title" => "Koltuk",
                "slug" => "Koltuk",
                "description" => "koltuk kategorisine ait açıklama metni.",
                "rank" => 1,
                "image" => null,
                "is_active" => 1,
                "parent_id" => 2,
                "created_at" => "2020-01-06 01:01:32"
            ]
        ];

        foreach ($data as $dt){
            DB::table("product_categories")->insert($dt);
        }
    }
}
