<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "name"  => "Murat CAN",
            "email" => "murat@stuket.com",
            "phone" => "0555 555 5555",
            "image" => "image",
            "is_active" => 1,
            "password" => Hash::make("123"),
            "about" => "About Murat CAN"
        ];

        DB::table("users")->insert($data);
    }
}
