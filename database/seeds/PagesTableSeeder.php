<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          [
              "title" => "Hakkımızda",
              "slug" => "hakkimizda",
              "description" => "Stuket projesine ait hakkımızda sayfası.",
              "content" => "Bitirme projesi olarak çıktığımız bu yolda büyümenin haklı gururunu yaşıyoruz.",
              "rank" => 3,
              "is_active" => 1


          ],
          [
              "title" => "S.S.S.",
              "slug" => "sikca-sorulan-sorular",
              "description" => "Sıkça sorulan sorular",
              "content" => "Sorular...",
              "rank" => 1,
              "is_active" => 1

          ]
        ];

        DB::table('pages')->insert($data);
    }
}
