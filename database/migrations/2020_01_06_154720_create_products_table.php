<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->string("slug");
            $table->text("description");
            $table->text("address");
            $table->decimal("price",9,2);
            $table->boolean("is_active");
            $table->unsignedBigInteger("user_id")->nullable();
            $table->unsignedBigInteger("category_id")->nullable();
            $table->unsignedBigInteger("university_id")->nullable();
            $table->unsignedBigInteger("faculty_id")->nullable();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("category_id")->references("id")->on("product_categories")->onDelete("cascade");
            $table->foreign("university_id")->references("id")->on("universities")->onDelete("cascade");
            $table->foreign("faculty_id")->references("id")->on("faculties")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
